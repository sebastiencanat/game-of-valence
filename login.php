<?php
session_start();
include('php/config.php');
$login_username = "";
$inscription_username="";
$inscription_couleur="#000000";
$inscription_nom_couleur="black";
$scriptJS_a_ajouter="";

if ($config["peut_inscrire"] == "1" && isset($_POST["action"]) && $_POST["action"] == "register" && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["color-value"]) && isset($_POST["color-name"]) && isset($_POST["territoire1"]) && isset($_POST["territoire2"])) {
	list($table1, $id1) = explode(",",$_POST["territoire1"]);
	list($table2, $id2) = explode(",",$_POST["territoire2"]);
	if (($table1 == "zones" || $table1 == "keypoints") && ($table2 == "zones" || $table2 == "keypoints") && is_numeric($id1) && is_numeric($id2)) {
		if(sqlexec("select proprietaire from ".$table1." where id=".$id1)[0]["proprietaire"] == "0" && sqlexec("select proprietaire from ".$table2." where id=".$id2)[0]["proprietaire"] == "0") {
			$sql="INSERT INTO `joueurs` (`nom`, `password`, `couleur`, `nom_couleur`) VALUES (".$conn->quote($_POST["username"]).", '".hash('sha512',$_POST["password"])."', ".$conn->quote($_POST["color-value"]).", ".$conn->quote($_POST["color-name"]).");
			SELECT LAST_INSERT_ID() INTO @Last_Id;
			UPDATE ".$table1." set proprietaire=@Last_Id where id=".$id1.";
			UPDATE ".$table2." set proprietaire=@Last_Id where id=".$id2.";";
			sqlexec($sql);
			$_POST["action"]="login";
			$scriptJS_a_ajouter='$("#section_inscription").toggleClass("style4 style5");document.getElementById("form_inscription").style.display = "none";document.getElementById("ssTitreInscription").innerHTML="Votre inscription à bien été enregistrée, vous pouvez à présent vous connecter";';
		} else {
			// Ohh tu t'es fait piqué les territoires que tu voulais mon pauvre choupinou :'( pas trop l'seum ?
			$inscription_username=$_POST["username"];
			$inscription_couleur=$_POST["color-value"];
			$inscription_nom_couleur="black";
			$scriptJS_a_ajouter='document.getElementById("ssTitreInscription").outerHTML="<p style=\'color:#FF2800;\'>Votre inscription a échouée car les territoires que vous avez choisis ont été conquis pendant votre inscription</p>";VerifySigninUsername();update_color();';
		}
	}
}
if (isset($_POST["action"]) && $_POST["action"] == "login" && isset($_POST["username"]) && isset($_POST["password"])) {
	$result = sqlexec("select count(id) as nbr, id, nom, password, couleur, last_login from joueurs where nom=".$conn->quote($_POST["username"])." group by id")[0];
	if ($result["nbr"] == 1) {
		if (hash('sha512',$_POST["password"]) == $result["password"]) {
            if ($config["peut_connecter"] == "1" || $result["id"] == "0") {
                $_SESSION['id'] = $result['id'];
                $_SESSION['nom'] = $result['nom'];
                $_SESSION['password'] = $result['password'];
                $_SESSION['couleur'] = $result['couleur'];
                $_SESSION['last_login'] = $result['last_login'];
                sqlexec("update joueurs set last_login=now() where id=".$result['id'].";");

                if (isset($_POST['cookie']) && $_POST['cookie'] == 'on'){
                    setcookie('id', $result['id'], time()+365*24*3600);
                    setcookie('password', $result['password'], time()+365*24*3600);
                }
                // Connexion OK
                header('Location: espace-joueurs.php');
                exit();
            } else { // Connexion interdite par admin (fin du jeu)
                $login_username=$result["nom"];
                $scriptJS_a_ajouter='document.getElementById("ssTitreLogin").outerHTML="<p style=\'color:#FF2800;\'>Désolé '.$result["nom"].', le jeu est fermé</p>";';
            }
		} else {
			// Erreur de mot de passe
			$login_username=$result["nom"];
			$scriptJS_a_ajouter='document.getElementById("ssTitreLogin").outerHTML="<p style=\'color:#FF2800;\'>Mot de passe incorrect</p>";InvalidLoginPassword();';
		}
	} else {
		// login inconnu
		$scriptJS_a_ajouter='document.getElementById("ssTitreLogin").outerHTML="<p style=\'color:#FF2800;\'>Nom de joueur incorrect</p>";InvalidLoginUsername();';
	}
}

$territoires_libres = sqlexec("select * from (
select 'keypoints' as sqltable, keypoints.id as id,keypoints.points as points, keypoints.nom as nom from keypoints where proprietaire='0'
union all
select 'zones' as sqltable, zones.id as id,zones.points as points, zones.nom as nom from zones where proprietaire='0'
) as territoires_attaquables order by points desc, nom asc");
if (count($territoires_libres) > 1 && $config["peut_inscrire"] == "1") {
	$ssTitreInscription="<p id='ssTitreInscription'>Les inscriptions sont possible tant qu'il reste au moins 2 territoires libres</p>";
	$select1="<select id='territoire1' name='territoire1' form='signin' onchange='javascript:updateSelect();'>";
	$select2="<select id='territoire2' name='territoire2' form='signin' onchange='javascript:updateSelect();'>";
	foreach ($territoires_libres as $territoire) {
		$select1.="<option value='".$territoire["sqltable"].",".$territoire["id"]."'>".$territoire["points"]." Point(s) - ".$territoire["nom"]."</option>";
		$select2.="<option value='".$territoire["sqltable"].",".$territoire["id"]."'>".$territoire["points"]." Point(s) - ".$territoire["nom"]."</option>";
	}
	$select1.="</select>";
	$select2.="</select>";
$form_inscription= <<<FORMULAIRE
<div id="form_inscription" class="container 50%">
	<form id="signin" action="#" method="post" onsubmit="return VerifFormInscription();" >
	<input name="action" value="register" type="hidden">
		<div class="row uniform">
			<div class="12u">
				<input name="username" id="signin-username" value="$inscription_username" placeholder="Nom du joueur" type="text" onchange='javascript:VerifySigninUsername();'>
			</div>
			<div class="12u">
				<input name="password" id="signin-password" value="" placeholder="Mot de passe" type="password">
			</div>
			</br></br></br></br>
			<input id="color-value" name="color-value" value="$inscription_couleur" type="color" onchange="javascript:update_color();" style="display: none;">
			<input id="color-name" type="hidden" name="color-name" value="$inscription_nom_couleur">
			<div id="error-color" class="row uniform" style="display:none;color:#FF2800;margin: 0 0 -1px -2em;">
				<p style="color:#FF2800;">Cette couleur est trop proche de celle c'un autre joueur : Choississez en une autre</p>
			</div>
			<div class="row uniform" style="margin: 0 0 -1px -2em;">
				<p>Choisissez votre couleur:</p>
			<div class="row uniform" style="float:none;margin-top:10%;">
				<b id="chosen-color" style="float:none;margin:auto;display: block; width: 40px; height: 40px; border-radius:50%; border: 1px solid; background-color: $inscription_couleur; cursor: pointer;" onclick="javascript:document.getElementById('color-value').click();"></b>
			</div>
			<div class="row uniform" style="margin: 0 0 -1px -2em;">
				<p>Choisissez vos 2 premiers territoires:</p>
			</div>
			<div id="select_territoire1" class="12u">
			$select1
			</div>
			<div id="select_territoire2" class="12u">
			$select2
			</div>
			<div class="12u$">
				<ul class="actions">
					<li><input value="S'inscrire" class="special big" type="submit"></li>
				</ul>
			</div>
		</div>
	</form>
	<script>$("#territoire1").children()[1].disabled=true;$("#territoire2").children()[0].disabled=true;$("#territoire2").children()[1].selected=true;$scriptJS_a_ajouter</script>
</div>
FORMULAIRE;
} else {
	$ssTitreInscription="<p id='ssTitreInscription'>Désolé, les inscriptions sont closes</p>";
	$form_inscription="";
}
if ($config["print_date_inscription"] != "0" && $config["print_date_fin_inscription"] != "0") {
    $regles_inscription = "Inscriptions ouvertes du $config[print_date_inscription] au $config[print_date_fin_inscription]</br>";
} else if ($config["print_date_inscription"] != "0") {
    $regles_inscription = "Inscriptions ouvertes a partir du $config[print_date_inscription]</br>";
} else if ($config["print_date_fin_inscription"] != "0") {
    $regles_inscription = "Inscriptions ouvertes jusqu'au $config[print_date_fin_inscription]</br>";
} else {
    $regles_inscription="";
}
if ($config["print_date_login"] != "0" && $config["print_date_end"] != "0") {
    $regles_login = "Le jeu est ouvert du $config[print_date_login] au $config[print_date_end]</br>";
} else if ($config["print_date_login"] != "0") {
    $regles_login = "Le jeu est ouvert a partir du $config[print_date_login]</br>";
} else if ($config["print_date_end"] != "0") {
    $regles_login = "Le jeu se termine le $config[print_date_end]</br>";
} else {
    $regles_login="";
}
?>
<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
	<?php echo $config["peut_inscrire"]; ?>
-->
<html lang="fr">
	<head>
	<!--<?php echo $config["peut_inscrire"]; ?> -->
		<meta charset="UTF-8">
		<title>Game of <?php echo $nom_ville; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<script src="js/login.js"></script>
		
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header">
				<h1><a href="index.php">Game of <?php echo $nom_ville; ?></a></h1>
				<nav id="nav">
					<ul>
						<li><a href="index.php#la_carte">La Carte</a></li>
						<li><a href="index.php#les_territoires">Les Territoires attaquables</a></li>
						<li><a href="index.php#le_classement">Le Classement</a></li>
						<li><a href="index.php#les_regles">Les Règles</a></li>
					</ul>
				</nav>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h2>Game of <?php echo $nom_ville; ?></h2>
				<p>Saurez vous conquérir <?php echo $nom_ville; ?>?</p>
			</section>
			
		<!-- One -->
			<section id="section_connexion" class="wrapper style3 special">
				<div class="container">
					<header class="major">
						<h2>Connectez vous !</h2>
						<p id="ssTitreLogin">Vous devez vous connecter pour pouvoir jouer</p>
                        <?php echo $regles_login; ?>
					</header>
				</div>
				<div class="container 50%">
					<form id="login" action="#" method="post">
					<input name="action" value="login" type="hidden">
						<div class="row uniform">
							<div class="12u">
								<input name="username" id="login-username" value="<?php echo $login_username; ?>" placeholder="Nom du joueur" type="text" onchange='javascript:document.getElementById("login-username").style.backgroundColor = "white";document.getElementById("login-username").placeholder = "Nom du joueur";'>
							</div>
						</div><div class="row uniform">
							<div class="12u">
								<input name="password" id="login-password" value="" placeholder="Mot de passe" type="password" onchange='javascriptdocument.getElementById("login-password").style.backgroundColor = "white";document.getElementById("login-password").placeholder = "Mot de passe";'>
							</div>
							<div class="12u$">
							<input type="checkbox" name="cookie" id="cookie" value="on"/> <label for="cookie">Me connecter automatiquement.</label>
								<ul class="actions">
									<li><input value="Connexion" class="special big" type="submit"></li>
								</ul>
							</div>
						</div>
					</form>
				</div>
			</section>
	
		<!-- two -->
			<section id="section_inscription" class="wrapper style4 special">
				<div class="container">
					<header class="major">
						<h2>Créer mon compte</h2>
						<?php echo $ssTitreInscription; ?>
						<?php echo $regles_inscription; ?>
					</header>
				</div>
				<?php echo $form_inscription; ?>
			</section>

		<?php echo $footer; ?>
	</body>
</html>