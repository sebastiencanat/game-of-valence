function getTerritoires_attaquables(){
	$.ajax({
	   type: "POST",
		url: "php/getTerritoires_attaquables.php",
		success: result, // si tout s'es bien pass鬠on execute la fonction "result"
		dataType: "json"
		});

		function result(data){
			if (data.length > 0){
				var tab_Territoires_attaquables;
				tab_Territoires_attaquables = "<table><thead><tr><th>Points</th><th>Territoire</th><th>Propriétaire</th><th></th></tr></thead><tbody>";
				data.forEach(function (territoire){
					tab_Territoires_attaquables += "<tr><td>"+territoire.points+"</td><td>"+territoire.nom+"</td><td>"+territoire.proprietaire+"</td><td><b style=\"display: block; width: 20px; height: 20px; background-color: "+territoire.couleur+"; margin:auto; border-radius:50%; border: 1px solid;\"></b></td></tr>";
				})
				tab_Territoires_attaquables += "</tbody></table>";
				document.getElementById("Territoires_attaquables").innerHTML = tab_Territoires_attaquables;
				if (data.length > 1){	
					document.getElementById("ssTitre_Territoires_attaquables").innerHTML = data.length+" territoires attaquables actuellement !";
				} else {
					document.getElementById("ssTitre_Territoires_attaquables").innerHTML = "Un seul territoire attaquable actuellement !";
				}
			} else {
				document.getElementById("ssTitre_Territoires_attaquables").innerHTML = "Il n'y a aucun territoire attaquable actuellement !";
			}
		}
}
getTerritoires_attaquables();
var refresh_classement = setInterval(getTerritoires_attaquables, 60000);

function getClassement(){
	$.ajax({
	   type: "POST",
		url: "php/getClassement.php",
		success: result, // si tout s'es bien pass鬠on execute la fonction "result"
		dataType: "json"
		});

		function result(data){
			var tab_Scores;
			var position;
			nbjoueurs=data.length;
			position = 0;
			tab_Scores = "<table><thead><tr><th>Score</th><th>Nom</th><th>Territoires</th><th></th></tr></thead><tbody>";
			data.forEach(function (joueur){
				position++;
				tab_Scores += "<tr><td>"+position+"/"+nbjoueurs+" : "+joueur.Score+"&nbsp;Pts</td><td>"+joueur.Nom+"</td><td>"+joueur.Territoires+"</td><td><b style=\"display: block; width: 20px; height: 20px; background-color: "+joueur.couleur+"; margin:auto; border-radius:50%; border: 1px solid;\"></b></td></tr>";
			})
			tab_Scores += "</tbody></table>";
			document.getElementById("Classement").innerHTML = tab_Scores;
		}
}
getClassement();
var refresh_classement = setInterval(getClassement, 60000);

//map
var map = L.map('map', {editable: true});
var maxBounds;
var est_calibre = false;
map.on('drag', function() {
    map.panInsideBounds(maxBounds, { animate: false });
});
//url des TMS de fond
var osm = L.tileLayer(tile_server, {attribution: tile_attribution,
    minZoom: 1,
    maxZoom: 18});

//on ajoute le fond OSM au départ
osm.addTo(map)
//on ajoute le 'control' des fonds de carte

var FGgpx = L.featureGroup().addTo(map); // feature group ou on va inserer nos traces
// la fonction getTrace() définie plus bas, génère un geojson et l'affiche dans leafLet, on l'execute au départ
getTrace();
function monStyle(feature) {
        return {color: "#000000", weight: "1", fillColor: feature.properties.couleur, fillOpacity: "0.7"};
}

function onEachFeature(feature, layer) {
	//on lui fait afficher une popup lors d'un click
		if (feature.properties.proprietaire_id == '0') {
            layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br>Territoire disponible');
        } else if (feature.properties.attaquant == "") {
            layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire);
        } else {
            layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire+'</br> Attaquant : ' +feature.properties.attaquant);
        }
    //on lui applique le style
        layer.setStyle(monStyle(feature));
}

function calibrage_map() {
    if (!est_calibre) {
        if (FGgpx.getLayers().length == 0) {
            map.setView([45,4],5);
        } else {
            map.fitBounds(FGgpx.getBounds());
            maxBounds = map.getBounds();
            map.setMaxBounds(maxBounds);
            map.options.minZoom = map.getZoom();
            document.getElementsByClassName('leaflet-control-zoom-out')[0].className += ' leaflet-disabled';
            est_calibre = true;
        }
    }
}
      
function getTrace(){

 $.ajax({
           type: "POST",
            url: "php/getTrace.php",
			success: result,
            dataType: 'json'
            });

			function result(data){
                FGgpx.clearLayers();
                data.features.forEach(function(feature){
                    var layer = L.polygon(L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates));
                    onEachFeature(feature, layer);
                    layer.addTo(FGgpx);
				})
			}
setTimeout(getMark, 50);		
}

function getMark(){
 $.ajax({
           type: "POST",
            url: "php/getMark.php",
			success: result,
            dataType: 'json'
            });

			function result(data){
                data.features.forEach(function(feature){
					var layer = L.circleMarker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates), {radius:8});
					onEachFeature(feature, layer);
					layer.addTo(FGgpx);
				})
			}
setTimeout(calibrage_map, 1000);
}

var refresh_trace = setInterval(getTrace, 60000);
function responsive() {
    if (window.innerHeight > window.innerWidth) {
        //si téléphone portrait
        map_container_height=$("#map").width()+"px";
    } else {
        map_container_height=window.innerHeight/3*2+"px";
    }
    document.getElementById("map_container").style.height = map_container_height;
    if (window.innerWidth < 1020) {
        document.getElementById("header_site_name").style.display = "none";
    }
}
window.onresize = responsive;
responsive();