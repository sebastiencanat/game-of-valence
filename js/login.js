function update_color(){
	document.getElementById('chosen-color').style.backgroundColor = document.getElementById('color-value').value;
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(document.getElementById('color-value').value);
	r = parseInt(result[1], 16);
	g = parseInt(result[2], 16);
	b = parseInt(result[3], 16);
	color_list = [{"r":"255","g":"250","b":"250","color":"snow"},{"r":"248","g":"248","b":"255","color":"GhostWhite"},{"r":"245","g":"245","b":"245","color":"WhiteSmoke"},{"r":"220","g":"220","b":"220","color":"gainsboro"},{"r":"255","g":"250","b":"240","color":"FloralWhite"},{"r":"253","g":"245","b":"230","color":"OldLace"},{"r":"250","g":"240","b":"230","color":"linen"},{"r":"250","g":"235","b":"215","color":"AntiqueWhite"},{"r":"255","g":"239","b":"213","color":"PapayaWhip"},{"r":"255","g":"235","b":"205","color":"BlanchedAlmond"},{"r":"255","g":"228","b":"196","color":"bisque"},{"r":"255","g":"218","b":"185","color":"PeachPuff"},{"r":"255","g":"222","b":"173","color":"NavajoWhite"},{"r":"255","g":"228","b":"181","color":"moccasin"},{"r":"255","g":"248","b":"220","color":"cornsilk"},{"r":"255","g":"255","b":"240","color":"ivory"},{"r":"255","g":"250","b":"205","color":"LemonChiffon"},{"r":"255","g":"245","b":"238","color":"seashell"},{"r":"240","g":"255","b":"240","color":"honeydew"},{"r":"245","g":"255","b":"250","color":"MintCream"},{"r":"240","g":"255","b":"255","color":"azure"},{"r":"240","g":"248","b":"255","color":"AliceBlue"},{"r":"230","g":"230","b":"250","color":"lavender"},{"r":"255","g":"240","b":"245","color":"LavenderBlush"},{"r":"255","g":"228","b":"225","color":"MistyRose"},{"r":"255","g":"255","b":"255","color":"white"},{"r":"0","g":"0","b":"0","color":"black"},{"r":"105","g":"105","b":"105","color":"DimGrey"},{"r":"112","g":"128","b":"144","color":"SlateGrey"},{"r":"119","g":"136","b":"153","color":"LightSlateGrey"},{"r":"190","g":"190","b":"190","color":"grey"},{"r":"211","g":"211","b":"211","color":"LightGray"},{"r":"25","g":"25","b":"112","color":"MidnightBlue"},{"r":"0","g":"0","b":"128","color":"NavyBlue"},{"r":"100","g":"149","b":"237","color":"CornflowerBlue"},{"r":"72","g":"61","b":"139","color":"DarkSlateBlue"},{"r":"106","g":"90","b":"205","color":"SlateBlue"},{"r":"123","g":"104","b":"238","color":"MediumSlateBlue"},{"r":"132","g":"112","b":"255","color":"LightSlateBlue"},{"r":"0","g":"0","b":"205","color":"MediumBlue"},{"r":"65","g":"105","b":"225","color":"RoyalBlue"},{"r":"0","g":"0","b":"255","color":"blue"},{"r":"30","g":"144","b":"255","color":"DodgerBlue"},{"r":"0","g":"191","b":"255","color":"DeepSkyBlue"},{"r":"135","g":"206","b":"235","color":"SkyBlue"},{"r":"135","g":"206","b":"250","color":"LightSkyBlue"},{"r":"70","g":"130","b":"180","color":"SteelBlue"},{"r":"176","g":"196","b":"222","color":"LightSteelBlue"},{"r":"173","g":"216","b":"230","color":"LightBlue"},{"r":"176","g":"224","b":"230","color":"PowderBlue"},{"r":"175","g":"238","b":"238","color":"PaleTurquoise"},{"r":"0","g":"206","b":"209","color":"DarkTurquoise"},{"r":"72","g":"209","b":"204","color":"MediumTurquoise"},{"r":"64","g":"224","b":"208","color":"turquoise"},{"r":"0","g":"255","b":"255","color":"cyan"},{"r":"224","g":"255","b":"255","color":"LightCyan"},{"r":"95","g":"158","b":"160","color":"CadetBlue"},{"r":"102","g":"205","b":"170","color":"MediumAquamarine"},{"r":"127","g":"255","b":"212","color":"aquamarine"},{"r":"0","g":"100","b":"0","color":"DarkGreen"},{"r":"85","g":"107","b":"47","color":"DarkOliveGreen"},{"r":"143","g":"188","b":"143","color":"DarkSeaGreen"},{"r":"46","g":"139","b":"87","color":"SeaGreen"},{"r":"60","g":"179","b":"113","color":"MediumSeaGreen"},{"r":"32","g":"178","b":"170","color":"LightSeaGreen"},{"r":"152","g":"251","b":"152","color":"PaleGreen"},{"r":"0","g":"255","b":"127","color":"SpringGreen"},{"r":"0","g":"255","b":"0","color":"green"},{"r":"127","g":"255","b":"0","color":"chartreuse"},{"r":"0","g":"250","b":"154","color":"MediumSpringGreen"},{"r":"173","g":"255","b":"47","color":"GreenYellow"},{"r":"50","g":"205","b":"50","color":"LimeGreen"},{"r":"154","g":"205","b":"50","color":"YellowGreen"},{"r":"34","g":"139","b":"34","color":"ForestGreen"},{"r":"107","g":"142","b":"35","color":"OliveDrab"},{"r":"189","g":"183","b":"107","color":"DarkKhaki"},{"r":"240","g":"230","b":"140","color":"khaki"},{"r":"238","g":"232","b":"170","color":"PaleGoldenrod"},{"r":"250","g":"250","b":"210","color":"LightGoldenrodYellow"},{"r":"255","g":"255","b":"224","color":"LightYellow"},{"r":"255","g":"255","b":"0","color":"yellow"},{"r":"255","g":"215","b":"0","color":"gold"},{"r":"238","g":"221","b":"130","color":"LightGoldenrod"},{"r":"218","g":"165","b":"32","color":"goldenrod"},{"r":"184","g":"134","b":"11","color":"DarkGoldenrod"},{"r":"188","g":"143","b":"143","color":"RosyBrown"},{"r":"205","g":"92","b":"92","color":"IndianRed"},{"r":"139","g":"69","b":"19","color":"SaddleBrown"},{"r":"160","g":"82","b":"45","color":"sienna"},{"r":"205","g":"133","b":"63","color":"peru"},{"r":"222","g":"184","b":"135","color":"burlywood"},{"r":"245","g":"245","b":"220","color":"beige"},{"r":"245","g":"222","b":"179","color":"wheat"},{"r":"244","g":"164","b":"96","color":"SandyBrown"},{"r":"210","g":"180","b":"140","color":"tan"},{"r":"210","g":"105","b":"30","color":"chocolate"},{"r":"178","g":"34","b":"34","color":"firebrick"},{"r":"165","g":"42","b":"42","color":"brown"},{"r":"233","g":"150","b":"122","color":"DarkSalmon"},{"r":"250","g":"128","b":"114","color":"salmon"},{"r":"255","g":"160","b":"122","color":"LightSalmon"},{"r":"255","g":"165","b":"0","color":"orange"},{"r":"255","g":"140","b":"0","color":"DarkOrange"},{"r":"255","g":"127","b":"80","color":"coral"},{"r":"240","g":"128","b":"128","color":"LightCoral"},{"r":"255","g":"99","b":"71","color":"tomato"},{"r":"255","g":"69","b":"0","color":"OrangeRed"},{"r":"255","g":"0","b":"0","color":"red"},{"r":"255","g":"105","b":"180","color":"HotPink"},{"r":"255","g":"20","b":"147","color":"DeepPink"},{"r":"255","g":"192","b":"203","color":"pink"},{"r":"255","g":"182","b":"193","color":"LightPink"},{"r":"219","g":"112","b":"147","color":"PaleVioletRed"},{"r":"176","g":"48","b":"96","color":"maroon"},{"r":"199","g":"21","b":"133","color":"MediumVioletRed"},{"r":"208","g":"32","b":"144","color":"VioletRed"},{"r":"255","g":"0","b":"255","color":"magenta"},{"r":"238","g":"130","b":"238","color":"violet"},{"r":"221","g":"160","b":"221","color":"plum"},{"r":"218","g":"112","b":"214","color":"orchid"},{"r":"186","g":"85","b":"211","color":"MediumOrchid"},{"r":"153","g":"50","b":"204","color":"DarkOrchid"},{"r":"148","g":"0","b":"211","color":"DarkViolet"},{"r":"138","g":"43","b":"226","color":"BlueViolet"},{"r":"160","g":"32","b":"240","color":"purple"},{"r":"147","g":"112","b":"219","color":"MediumPurple"},{"r":"216","g":"191","b":"216","color":"thistle"}];
	dist = 255 * Math.sqrt(3.0);
	nmatches = 0;
	nom_couleur = '';
	color_list.forEach(function (couleur){
		if ( r == couleur.r && g == couleur.g && b == couleur.b ){
			nmatches++;
			nom_couleur = couleur.color;
		}
		if (nmatches == 0) {
			newdist = Math.sqrt(Math.pow(r-couleur.r, 2) + Math.pow(g-couleur.g, 2) + Math.pow(b-couleur.b, 2));
			if (newdist == dist) {
				nom_couleur = couleur.color;
			} else if (newdist < dist) {
				dist = newdist;
				nom_couleur = couleur.color;
			}
		}
	})
	document.getElementById("color-name").value = nom_couleur;
	document.getElementById("error-color").style.display = "none";
	VerifySigninColor();
}
VeritySigninUsernameOK = false;
function ResultVeritySigninUsername(bool) {
	VeritySigninUsernameOK = bool;
}
VerityColorOK = false;
function ResultVerityColor(bool) {
	VerityColorOK = bool;
}

function VerifySigninUsername(){
	$.ajax({
	   type: "POST",
		url: "php/getUsers_color.php",
		success: result,
		dataType: "json"
		});

	function result(data){
		test = true;
		document.getElementById("signin-username").style.backgroundColor = "white";
		document.getElementById("signin-username").placeholder = "Nom du joueur";
		data.forEach(function(user) {
			if (document.getElementById("signin-username").value.length > 20 || document.getElementById("signin-username").value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase() == user.nom.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()) {
				document.getElementById("signin-username").style.backgroundColor = "#FF5E41";
				document.getElementById("signin-username").value = "";
				document.getElementById("signin-username").placeholder = "Nom indisponible";
				test = false;
			}			
		})
		ResultVeritySigninUsername(test);
	}
}
function InvalidLoginUsername() {
	document.getElementById("login-username").style.backgroundColor = "#FF5E41";
	document.getElementById("login-username").value = "";
	document.getElementById("login-username").placeholder = "Nom Inconnu";
}
function InvalidLoginPassword() {
	document.getElementById("login-password").style.backgroundColor = "#FF5E41";
	document.getElementById("login-password").value = "";
	document.getElementById("login-password").placeholder = "Mot de passe Incorrect";
}

function VerifySigninColor(){
	$.ajax({
	   type: "POST",
		url: "php/getUsers_color.php",
		success: result,
		dataType: "json"
		});

	function result(data){
		test = true;
		data.forEach(function(user) {
			if (document.getElementById("color-name").value == user.nom_couleur || document.getElementById("color-name").value == "white") {
				document.getElementById("error-color").style.display = "block";
				test=false;
			}
			
		})
		ResultVerityColor(test);
	}
}


function VerifFormInscription(){
	if (!VeritySigninUsernameOK){
		document.getElementById("signin-username").style.backgroundColor = "#FF5E41";
		document.getElementById("signin-username").value = "";
		document.getElementById("signin-username").placeholder = "Nom indisponible";
	}
	if (!VerityColorOK) {
		document.getElementById("error-color").style.display = "block";
	}
	return VeritySigninUsernameOK && VerityColorOK;
}

function updateSelect() {
		$("#territoire1").children().each(function (){
			this.disabled = false;
		})
		$("#territoire2").children().each(function (){
			this.disabled = false;
		})
		$("option[value='"+document.getElementById("territoire1").value+"']")[1].disabled = true;
		$("option[value='"+document.getElementById("territoire2").value+"']")[0].disabled = true;
}