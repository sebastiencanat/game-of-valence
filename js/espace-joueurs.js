var keypoints_attaquables = [];
var zones_attaquables = [];
function getTerritoires_attaquables(){
	$.ajax({
	   type: "POST",
		url: "php/getTerritoires_attaquables.php",
		success: result, // si tout s'es bien pass鬠on execute la fonction "result"
		dataType: "json"
		});

		function result(data){
			if (data.length > 0){
				var tab_Territoires_attaquables;
				tab_Territoires_attaquables = "<table><thead><tr><th>Points</th><th>Territoire</th><th>Propriétaire</th><th></th></tr></thead><tbody>";
				data.forEach(function (territoire){
					tab_Territoires_attaquables += "<tr><td>"+territoire.points+"</td><td>"+territoire.nom+"</td><td>"+territoire.proprietaire+"</td><td><b style=\"display: block; width: 20px; height: 20px; background-color: "+territoire.couleur+"; margin:auto; border-radius:50%; border: 1px solid;\"></b></td></tr>";
				})
				tab_Territoires_attaquables += "</tbody></table>";
				document.getElementById("Territoires_attaquables").innerHTML = tab_Territoires_attaquables;
				if (data.length > 1){	
					document.getElementById("ssTitre_Territoires_attaquables").innerHTML = data.length+" territoires attaquables actuellement !";
				} else {
					document.getElementById("ssTitre_Territoires_attaquables").innerHTML = "Un seul territoire attaquable actuellement !";
				}
			} else {
				document.getElementById("ssTitre_Territoires_attaquables").innerHTML = "Il n'y a aucun territoire attaquable actuellement !";
			}
		}
}
getTerritoires_attaquables();
var refresh_classement = setInterval(getTerritoires_attaquables, 60000);

function getClassement(){
	$.ajax({
	   type: "POST",
		url: "php/getClassement.php",
		success: result,
		dataType: "json"
		});

		function result(data){
			var tab_Scores;
			var position;
			nbjoueurs=data.length;
			position = 0;
			tab_Scores = "<table><thead><tr><th>Score</th><th>Nom</th><th>Territoires</th><th></th></tr></thead><tbody>";
			data.forEach(function (joueur){
				position++;
				tab_Scores += "<tr><td>"+position+"/"+nbjoueurs+" : "+joueur.Score+"&nbsp;Pts</td><td>"+joueur.Nom+"</td><td>"+joueur.Territoires+"</td><td><b style=\"display: block; width: 20px; height: 20px; background-color: "+joueur.couleur+"; margin:auto; border-radius:50%; border: 1px solid;\"></b></td></tr>";
			})
			tab_Scores += "</tbody></table>";
			document.getElementById("Classement").innerHTML = tab_Scores;
		}
}
getClassement();
var refresh_classement = setInterval(getClassement, 60000);

//map
var map = L.map('map');
var maxBounds;
var est_calibre = false;
map.on('drag', function() {
    map.panInsideBounds(maxBounds, { animate: false });
});
//url des TMS de fond
var osm = L.tileLayer(tile_server, {attribution: tile_attribution,
    minZoom: 1,
    maxZoom: 18});

//on ajoute le fond OSM au départ
osm.addTo(map)
//on ajoute le 'control' des fonds de carte

var FGgpx = L.featureGroup().addTo(map); // feature group ou on va inserer nos traces

// la fonction getTrace() définie plus bas, génère un geojson et l'affiche dans leafLet, on l'execute au départ
getTrace();
function monStyle(feature) {
        return {color: "#000000", weight: "1", fillColor: feature.properties.couleur, fillOpacity: "0.7"};
}

function onEachFeature(feature, layer, type = "Zone") {
    //on lui fait afficher une popup lors d'un click avec bouton pour revendiquer si possible
    if (type == "Zone") {
        if (zones_attaquables.indexOf(feature.properties.id) != -1) {
            if (feature.properties.proprietaire_id == '0') {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br>Territoire disponible </br></br> <p class="button small BtnRevendiquer" style="display:none;" onclick="revendiquer_from_map(\''+feature.properties.id+'\',\''+type+'\');">Revendiquer</p> ');
            } else if (feature.properties.attaquant == "") {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire+'</br></br> <p class="button small BtnRevendiquer" style="display:none;" onclick="revendiquer_from_map(\''+feature.properties.id+'\',\''+type+'\');">Revendiquer</p> ');
            } else {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire+'</br> Attaquant : ' +feature.properties.attaquant+'</br></br> <p class="button small BtnRevendiquer" style="display:none;" onclick="revendiquer_from_map(\''+feature.properties.id+'\',\''+type+'\');">Revendiquer</p> ');
            }
        } else {
            if (feature.properties.proprietaire_id == '0') {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br>Territoire disponible');
            } else if (feature.properties.attaquant == "") {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire);
            } else {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire+'</br> Attaquant : ' +feature.properties.attaquant);
            }
        }
    } else {
        if (keypoints_attaquables.indexOf(feature.properties.id) != -1) {
            if (feature.properties.proprietaire_id == '0') {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br>Territoire disponible </br></br> <p class="button small BtnRevendiquer" style="display:none;" onclick="revendiquer_from_map(\''+feature.properties.id+'\',\''+type+'\');">Revendiquer</p> ');
            } else if (feature.properties.attaquant == "") {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire+'</br></br> <p class="button small BtnRevendiquer" style="display:none;" onclick="revendiquer_from_map(\''+feature.properties.id+'\',\''+type+'\');">Revendiquer</p> ');
            } else {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire+'</br> Attaquant : ' +feature.properties.attaquant+'</br></br> <p class="button small BtnRevendiquer" style="display:none;" onclick="revendiquer_from_map(\''+feature.properties.id+'\',\''+type+'\');">Revendiquer</p> ');
            }
        } else {
            if (feature.properties.proprietaire_id == '0') {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br>Territoire disponible ');
            } else if (feature.properties.attaquant == "") {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire);
            } else {
                layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+'</br> Propri&eacute;taire : ' +feature.properties.proprietaire+'</br> Attaquant : ' +feature.properties.attaquant);
            }
        }
    }
    //on lui applique le style
        layer.setStyle(monStyle(feature));
}

function revendiquer_from_map(id,type) {
    if (type == "Zone") {
        document.getElementById("select-zone").value = id;
        document.getElementById("revendiquer_zone").submit();
    } else {
        document.getElementById("select-keypoint").value = id;
        document.getElementById("revendiquer_keypoint").submit();
    }
}

function calibrage_map() {
    if (!est_calibre) {
        if (FGgpx.getLayers().length == 0) {
            map.setView([45,4],5);
        } else {
            map.fitBounds(FGgpx.getBounds());
            maxBounds = map.getBounds();
            map.setMaxBounds(maxBounds);
            map.options.minZoom = map.getZoom();
            document.getElementsByClassName('leaflet-control-zoom-out')[0].className += ' leaflet-disabled';
            est_calibre = true;
        }
    }
}
      
function getTrace(){
getZones_attaquables_par_joueur();
getKeypoints_attaquables_par_joueur();
 $.ajax({
           type: "POST",
            url: "php/getTrace.php",
			success: result,
            dataType: 'json'
            });

			function result(data){
                FGgpx.clearLayers();
                data.features.forEach(function(feature){
                    var layer = L.polygon(L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates));
                    onEachFeature(feature, layer);
                    layer.addTo(FGgpx);
				})
			}
setTimeout(getMark, 50);		
}

function getMark(){
 $.ajax({
           type: "POST",
            url: "php/getMark.php",
			success: result,
            dataType: 'json'
            });

			function result(data){
                data.features.forEach(function(feature){
					var layer = L.circleMarker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates), {radius:8});
					onEachFeature(feature, layer);
					layer.addTo(FGgpx);
				})
			}
setTimeout(calibrage_map, 1000);
}
var refresh_trace = setInterval(getTrace, 60000);

function getResumeJoueur(){
	$.ajax({
		type: "POST",
		url: "php/getClassement.php",
		success: result,
		dataType: "json"
	});

	function result(data){
		var tab_Scores;
		var position;
		nbjoueurs=data.length;
		position = 0;
		data.forEach(function (joueur){
			position++;
			if (joueur.id == SessionId) {
				ResumeJoueur = "Classement: "+position+"/"+nbjoueurs+" avec un score de "+joueur.Score+"&nbsp; points.";
                if (joueur.point_penalite > "0") {
                    ResumeJoueur=ResumeJoueur+"</br><span style='color:red;'>Vous avez "+joueur.point_penalite+" points de pénalité.</span>";
                }
                if (joueur.point_penalite < "0") {
                    ResumeJoueur=ResumeJoueur+"</br><span style='color:green;'>Le MJ vous a accordé "+joueur.point_penalite*-1+" points de bonus.</span>";
                }
				Territoires_conquis = joueur.Territoires;
			}
		})
		document.getElementById("resume-joueur").innerHTML = ResumeJoueur;
		document.getElementById("vos_territoires").innerHTML = "Vos territoires: "+Territoires_conquis;
	}
}
getResumeJoueur();
var refresh_ResumeJoueur = setInterval(getResumeJoueur, 60000);



// liste des points clés attaquables
function getKeypoints_attaquables_par_joueur(){
	$.ajax({
		type: "POST",
		url: "php/getKeypoints_attaquables_par_joueur.php",
		success: result,
		dataType: "json"
	});

	function result(data){
        keypoints_attaquables = [];
		if (data.length > 0){
            document.getElementById("form_revendiquer_keypoint").style.display="block";
			selectKeypoint="<select id='select-keypoint' name='keypoint' form='revendiquer_keypoint'><option value='0' selected disabled>Sélectionnez un point clé à revendiquer</option>";
			data.forEach(function (keypoint){
				if (keypoint.proprietaire == "0") {
					defier="";
				} else {
					defier=" (défier "+keypoint.nom_proprietaire+")";
				}
				selectKeypoint=selectKeypoint+"<option value='"+keypoint.id+"'>"+keypoint.points+" Point(s) - "+keypoint.nom+defier+"</option>";
                keypoints_attaquables.push(keypoint.id);
			})
			selectKeypoint=selectKeypoint+"</select>";
			document.getElementById("select_keypoint").innerHTML = selectKeypoint;
		}
	}
}



// liste des zones attaquables
function getZones_attaquables_par_joueur(){
	$.ajax({
		type: "GET",
		url: "php/getZones_attaquables_par_joueur.php?",
		success: result,
		dataType: "json"
	});

	function result(data){
        zones_attaquables = [];
        if (data.length > 0){
			document.getElementById("form_revendiquer_zone").style.display="block";
			selectZone="<select id='select-zone' name='zone' form='revendiquer_zone'><option value='0' selected disabled>Sélectionnez une zone à revendiquer</option>";
			data.forEach(function (zone){
				if (zone.proprietaire == "0") {
					defier="";
				} else {
					defier=" (défier "+zone.nom_proprietaire+")";
				}
				selectZone=selectZone+"<option value='"+zone.id+"'>"+zone.points+" Point(s) - "+zone.nom+defier+"</option>";
                zones_attaquables.push(zone.id);
			})
			selectZone=selectZone+"</select>";
			document.getElementById("select_zone").innerHTML = selectZone;
		}
	}
}

// peut il revendiquer un territoire ?
function canRevendiquer(){
	$.ajax({
		type: "GET",
		url: "php/canRevendiquer.php",
		success: result,
		dataType: "json"
	});

	function result(data){
		if (data.peut_revendiquer == "1") {
			document.getElementById("ssTitreRevendicatipons").innerHTML = "Etendez votre empire!";
			document.getElementById("formulaires_revendiquer").style.display = "block";
            for (var i = 0; i < document.getElementsByClassName("BtnRevendiquer").length; i++) {
                document.getElementsByClassName("BtnRevendiquer")[i].style = "background-color: #00cf46;margin:auto;display:block;";
            }
		} else {
			document.getElementById("formulaires_revendiquer").style.display = "none";
			document.getElementById("ssTitreRevendicatipons").innerHTML = "Vous ne pouvez pas revendiquer de territoires pour le moment";
            for (var i = 0; i < document.getElementsByClassName("BtnRevendiquer").length; i++) {
                document.getElementsByClassName("BtnRevendiquer")[i].style = "display:none;";
            }
		}
	}
}
canRevendiquer();
var refresh_Revendiquer = setInterval(canRevendiquer, 1000);

// liste les duels reçus
function getDuels_recus(){
	$.ajax({
	   type: "POST",
		url: "php/getDuels_recus.php",
		success: result,
		dataType: "json"
		});

		function result(data){
			if (data.length > 0) {
				compteur=0;
				tab_Duels_recus = "<table><thead><tr><th>Date</th><th>Nom de l'attaquant</th><th>Territoire</th><th>Type de défi</th></tr></thead><tbody>";
				data.forEach(function (duel){
					compteur++;
					if ( (duel.status == "NEW" && duel.defi_def == "1" ) || (duel.status == "ATTAQUANT_AVIS")) {
						tab_Duels_recus += "<tr onclick=\"document.location='duel.php?id="+duel.id+"#action_necessaire'\" style='cursor:pointer;background-color: #A52A2A;color:white;'>";
					} else if (duel.status == "DEFI") {
						tab_Duels_recus += "<tr onclick=\"document.location='duel.php?id="+duel.id+"#action_necessaire'\" style='cursor:pointer;background-color: #00CF46;color:white;'>";
					} else {
						tab_Duels_recus += "<tr onclick=\"document.location='duel.php?id="+duel.id+"'\" style='cursor:pointer;'>";
					}
					tab_Duels_recus += "<td>"+duel.update_date+"</td><td>"+duel.attaquant_nom+"</td><td>"+duel.type_territoire+" "+duel.territoire_nom+" ("+duel.territoire_points+" Points)</td><td>"+duel.defi_type+"</td></tr>";
				})
				tab_Duels_recus += "</tbody></table>";
				document.getElementById("duels_recus").innerHTML = tab_Duels_recus;
				if (compteur == 1){
					document.getElementById("nbDuelsRecus").innerHTML = compteur+" Duel reçu";
				} else {
					document.getElementById("nbDuelsRecus").innerHTML = compteur+" Duels reçus";
				}
			} else {
				document.getElementById("duels_recus").innerHTML = "<p>Vous n'êtes pas la cible de duels pour le moment</p>"
				document.getElementById("nbDuelsRecus").innerHTML = "0 Duel reçu";
			}
		}
}
getDuels_recus();
var refresh_Duels_recus = setInterval(getDuels_recus, 60000);

// liste les duels envoyés
function getDuels_envoyes(){
	$.ajax({
	   type: "POST",
		url: "php/getDuels_envoyes.php",
		success: result,
		dataType: "json"
		});

		function result(data){
			if (data.length > 0) {
				compteur=0;
				tab_Duels_envoyes = "<table><thead><tr><th>Date</th><th>Nom du défenseur</th><th>Territoire</th><th>Type de défi</th></tr></thead><tbody>";
				data.forEach(function (duel){
					compteur++;
					if ( (duel.status == "NEW" && duel.defi_atk == "1" ) || (duel.status == "DEFENSEUR_AVIS")) {
						tab_Duels_envoyes += "<tr onclick=\"document.location='duel.php?id="+duel.id+"#action_necessaire'\" style='cursor:pointer;background-color: #A52A2A;color:white;'>";
					} else if (duel.status == "DEFI") {
						tab_Duels_envoyes += "<tr onclick=\"document.location='duel.php?id="+duel.id+"#action_necessaire'\" style='cursor:pointer;background-color: #00CF46;color:white;'>";
					} else {
						tab_Duels_envoyes += "<tr onclick=\"document.location='duel.php?id="+duel.id+"'\" style='cursor:pointer;'>";
					}
					tab_Duels_envoyes += "<td>"+duel.update_date+"</td><td>"+duel.defenseur_nom+"</td><td>"+duel.type_territoire+" "+duel.territoire_nom+" ("+duel.territoire_points+" Points)</td><td>"+duel.defi_type+"</td></tr>";
				})
				tab_Duels_envoyes += "</tbody></table>";
				document.getElementById("duels_envoyes").innerHTML = tab_Duels_envoyes;
				if (compteur == 1){
					document.getElementById("nbDuelsEnvoyes").innerHTML = compteur+" Duel envoyé";
				} else {
					document.getElementById("nbDuelsEnvoyes").innerHTML = compteur+" Duels envoyés";
				}
			} else {
				document.getElementById("duels_envoyes").innerHTML = "<p>Vous n'avez pas duels en cours</p>"
				document.getElementById("nbDuelsEnvoyes").innerHTML = "0 Duel envoyé";
			}
		}
}
getDuels_envoyes();
var refresh_Duels_envoyes = setInterval(getDuels_envoyes, 60000);

// liste tous les duels en cours
function getDuels(){
	$.ajax({
	   type: "POST",
		url: "php/getDuels.php",
		success: result,
		dataType: "json"
		});

		function result(data){
			if (data.length > 0) {
				compteur=0;
				tab_Duels_envoyes = "<table><thead><tr><th>Date</th><th>Nom de l'attaquant</th><th>Nom du défenseur</th><th>Territoire</th></tr></thead><tbody>";
				data.forEach(function (duel){
					compteur++;
					tab_Duels_envoyes += "<tr onclick=\"document.location='duel.php?id="+duel.id+"'\" style='cursor:pointer;'><td>"+duel.update_date+"</td><td>"+duel.attaquant_nom+"</td><td>"+duel.defenseur_nom+"</td><td>"+duel.type_territoire+" "+duel.territoire_nom+" ("+duel.territoire_points+" Points)</td></tr>";
				})
				tab_Duels_envoyes += "</tbody></table>";
				document.getElementById("duels").innerHTML = tab_Duels_envoyes;
				if (compteur == 1){
					document.getElementById("nbDuels").innerHTML = compteur+" Duel en cours";
				} else {
					document.getElementById("nbDuels").innerHTML = compteur+" Duels en cours";
				}
			} else {
				document.getElementById("duels").innerHTML = "<p>Aucun duels en cours</p>"
			}
		}
}
getDuels();
var refresh_Duels = setInterval(getDuels, 60000);
function responsive() {
    if (window.innerHeight > window.innerWidth) {
        //si téléphone portrait
        map_container_height=$("#map").width()+"px";
    } else {
        map_container_height=window.innerHeight/3*2+"px";
    }
    document.getElementById("map_container").style.height = map_container_height;
    if (window.innerWidth < 1140) {
        document.getElementById("header_site_name").style.display = "none";
    }
}
window.onresize = responsive;
responsive();