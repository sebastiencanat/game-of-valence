# Description
Petit jeu en PHP, défier ses amis et conquérir des territoires a la manière de Risk

# Installation
* Cloner le repo
* importer le fichier default.sql dans votre base mysql
* compléter les variables dans php/config.default.php et le renommer config.php
* changer le mot de passe admin qui est vide par défaut
* configurer les règles du jeu via le compte admin
* Jouer

# Personnalisation

Vous pouvez changer le lieu du jeu, pour cela :

* allez via le compte admin sur la page éditer la carte.
* modifier le nom de la ville dans le fichier php/config.php

Vous pouvez ajouter des nouvelles zones de jeu via la carte interactive.

Attention, après avoir créé une zone vous devez l'éditer pour définir ses zones voisines

Pour ajouter un point clé, il est nécessaire que celui ci soit inclu dans une zone préexistante


