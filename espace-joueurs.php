<?php
session_start();
include('php/config.php');
actualiser_session();
if ($_SESSION["id"] == 0) {
	header('Location: admin.php');
	exit();
}
$retourRevendications="";
if (isset($_POST["action"]) && $_POST["action"] == "revendiquer" && ( (isset($_POST["zone"]) && is_numeric($_POST["zone"])) or (isset($_POST["keypoint"]) && is_numeric($_POST["keypoint"])) ) ) {
	$result = sqlexec("select (
    (
        not (
            (
                select IFNULL(now() > date_eliminatoires,1) from config
            ) and not (
                select count(*) > 0 from (
                    select id from zones where proprietaire=$_SESSION[id] union all select id from keypoints where proprietaire=$_SESSION[id]
                ) as territoires
            )
        )
    ) and (
        (
            FIND_IN_SET(DAYNAME(now()),(select jours_revendication from config))
        ) and (
                (
                ifnull(date(last_revendication), 1) < DATE_SUB(now(), INTERVAL $config[delais_revendications] DAY)
            ) or (
                num_revendication < $config[nb_revendication]
            )
        )
    )
) as peut_revendiquer from joueurs where id=$_SESSION[id];")[0];
	if ($result["peut_revendiquer"] == "1"){
		if (isset($_POST["zone"])) {
			$table="zone";
			$id_temp=intval($_POST["zone"]);
            if (sqlexec("select $id_temp in (select * from (
    select zones.id from keypoints left join zones on keypoints.zone_id = zones.id left join joueurs on zones.proprietaire = joueurs.id where zones.duel_id is null and zones.proprietaire!=$_SESSION[id] and keypoints.proprietaire=$_SESSION[id]
    union
    select zones.id from frontieres left join zones on frontieres.id2=zones.id left join joueurs on zones.proprietaire = joueurs.id where zones.duel_id is null and zones.proprietaire!=$_SESSION[id] and frontieres.id1 in (select id from zones where proprietaire=$_SESSION[id])
    union
    select zones.id from frontieres left join zones on frontieres.id1=zones.id left join joueurs on zones.proprietaire = joueurs.id where zones.duel_id is null and zones.proprietaire!=$_SESSION[id] and frontieres.id2 in (select id from zones where proprietaire=$_SESSION[id])
    ) as zones_attaquables) as zone_attaquable")[0]["zone_attaquable"] == "1") {
                $id = $id_temp;
            } else {
                $id = 0;
            }
		} else {
			$table="keypoint";
			$id=intval($_POST["keypoint"]);
		}
		$verif = sqlexec("select count(*) as verif, @proprio:=proprietaire from ".$table."s where id=".$id." and duel_id is null FOR UPDATE;INSERT INTO `duels` (`attaquant`, `defenseur`, `".$table."_id`) VALUES ('".$_SESSION["id"]."', @proprio, '".$id."');UPDATE ".$table."s set duel_id=LAST_INSERT_ID() where id=".$id." and duel_id is null;")[0]["verif"];
		if ($verif == 1){
			sqlexec("update joueurs set last_revendication=now(), num_revendication=num_revendication+1 where id=".$_SESSION["id"]);
			header('Location: espace-joueurs.php?revendication=ok#revendications');
			exit();
		} else {
			header('Location: espace-joueurs.php?revendication=ko#revendications');
			exit();
		}
	}
	header('Location: espace-joueurs.php');
	exit();
}
if (isset($_GET["revendication"]) && $_GET["revendication"] == "ok") {
	$retourRevendications="<p style='color:#00CF46;'>Votre revendication à bien été prise en compte</p>";
} else if (isset($_GET["revendication"]) && $_GET["revendication"] == "ko") {
	$retourRevendications="<p style='color:#FF2800;'>Votre revendication n'a pas été prise en compte car un autre joueur vous a pris de vitesse ...</p>";
}

?>
<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Espace joueurs - Game of <?php echo $nom_ville; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
        <link rel="stylesheet" href="css/leaflet.css" />
        <script src="js/leaflet.js"></script>
        <script>
        var SessionId = <?php echo $_SESSION["id"] ?>;
        var tile_server = '<?php echo $config["tile_server"] ?>';
        var tile_attribution = '<?php echo $config["tile_attribution"] ?>';
        </script>
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header">
			<h1 id="header_site_name"><a href="espace-joueurs.php">Game of <?php echo $nom_ville; ?></a></h1>
				<nav id="nav">
					<ul>
						<li><a href="#revendications">Revendiquer un territoire</a></li>
						<li><a href="#mes_duels">Mes Duels</a></li>
						<li><a href="#le_classement">Le Classement</a></li>
						<li><a href="#les_regles">Les Règles</a></li>
						<li><a href="mon-compte.php">Mon Compte</a></li>
						<li><a href="logout.php" class="button special">Déconnexion</a></li>
					</ul>
				</nav>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h2 style="margin:0;">Bonjour, <?php echo $_SESSION["nom"]; ?></h2><b id="chosen-color" style="float:none;margin:auto;display: block; width: 80px; height: 80px; border-radius:50%; border: 1px solid; background-color: <?php echo $_SESSION["couleur"]; ?>;"></b>
				<p id="resume-joueur">Saurez vous conquérir <?php echo $nom_ville; ?>?</p>
			</section>

		<!-- One -->
			<section id="la_carte" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>La carte</h2>
						<p>mise à jour en temps réel !</p>
					</header>
					<div id="map_container" class="row container" style="margin:auto;padding:0;">
						<div id="map" ></div>
					</div>
				</div>
			</section>
		<!-- Two -->
			<section id="revendications" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2>Revendiquer un territoire</h2>
						<?php echo $retourRevendications; ?>
						<p id="ssTitreRevendicatipons">Etendez votre empire!</p>
						<p id="vos_territoires">Etendez votre empire!</p>
					</header>
				</div>
				<div id="formulaires_revendiquer">
					<div id="form_revendiquer_keypoint" class="container 50%" style="display:none;">
						<form id="revendiquer_keypoint" action="#" method="post">
							<input name="action" value="revendiquer" type="hidden">
							<div class="row uniform">
								<div class="row uniform">
									<p>Revendiquer un point clé:</p>
								</div>
								<div id="select_keypoint" class="12u">
								</div>
								<div class="12u$">
									<ul class="actions">
										<li><input value="Revendiquer ce point clé" class="special" type="submit"></li>
									</ul>
								</div>
							</div>
						</form>
					</div>
					<div id="form_revendiquer_zone" class="container 50%" style="display:none;">
						<form id="revendiquer_zone" action="#" method="post">
							<input name="action" value="revendiquer" type="hidden">
							<div class="row uniform">
								<div class="row uniform">
									<p>Revendiquer une zone:</p>
								</div>
								<div id="select_zone" class="12u">
								</div>
								<div class="12u$">
									<ul class="actions">
										<li><input value="Revendiquer cette zone" class="special" type="submit"></li>
									</ul>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>
		<!-- Three -->
			<section id="mes_duels" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Mes Duels</h2>
						<p>Les lignes rouges nécessitent une action de votre part, cliquez dessus!</p>
						<p>Les lignes vertes sont les défis que vous devez relever: cliquer pour définir le vainqueur!</p>
					</header>
					<h1 id="nbDuelsRecus">Duels reçus</h1>
					<div class="row" id="duels_recus">
					</div>
					<h1 id="nbDuelsEnvoyes">Duels envoyés</h1>
					<div class="row" id="duels_envoyes">
					</div>
				</div>
			</section>
		<!-- four -->
			<section id="tous_les_duels" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2>Tous les duels en cours</h2>
						<p id="nbDuels">Gardez un œil sur vos adversaires!</p>
					</header>
					<div class="row" id="duels">
					</div>
				</div>
			</section>
		<!-- Five -->			
			<section id="le_classement" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Le classement</h2>
						<p>mis à jour en temps réel !</p>
					</header>
					<section class="profiles">
						<div class="row" id="Classement">
						</div>
					</section>
				</div>
			</section>
			<section id="les_territoires" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2>Liste des territoires attaquables</h2>
						<p id="ssTitre_Territoires_attaquables"></p>
					</header>
					<section class="profiles">
						<div id="Territoires_attaquables">
						</div>
					</section>
				</div>
			</section>
		<!-- Six -->
			<?php echo $regles; ?>
		<?php echo $footer; ?>
	<script src="js/espace-joueurs.js"></script>
	</body>
</html>