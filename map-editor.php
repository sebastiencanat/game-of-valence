<?php
session_start();
include('php/config.php');
actualiser_session();

if ($_SESSION["id"] != 0) {
	header('Location: espace-joueurs.php');
	exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_tile" && isset($_POST["server"]) && isset($_POST["attribution"]) && $_POST["server"] != "" && $_POST["attribution"] != "" ) {
    sqlexec("update config set tile_server = '$_POST[server]', tile_attribution = '$_POST[attribution]'");
    header('Location: map-editor.php');
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "delete_zone" && isset($_POST["id"]) ) {
    sqlexec("delete from frontieres where id1=".$conn->quote($_POST["id"])." or id2=".$conn->quote($_POST["id"])."; delete from zones where id=".$conn->quote($_POST["id"]));
    header('Location: map-editor.php');
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "delete_keypoint" && isset($_POST["id"]) ) {
    sqlexec("delete from keypoints where id=".$conn->quote($_POST["id"]));
    header('Location: map-editor.php');
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "edit_zone" && isset($_POST["nom"])  && isset($_POST["points"]) && isset($_POST["geo"]) && isset($_POST["id"]) && isset($_POST["zone"]) && isset($_POST["geo"]) ) {
    $voisins = json_decode($_POST["zone"]);
    sqlexec("delete from frontieres where id1=".$conn->quote($_POST["id"])." or id2=".$conn->quote($_POST["id"]));
    foreach ($voisins as $voisin) {
        sqlexec("insert into frontieres values (".$conn->quote($_POST["id"]).",".$conn->quote($voisin).")");
    }
    sqlexec("update zones set nom=".$conn->quote($_POST["nom"]).", points=".$conn->quote($_POST["points"]).", geo=".$conn->quote(substr($_POST["geo"],1,-1))." where id=".$conn->quote($_POST["id"]));
    header('Location: map-editor.php');
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "edit_keypoint" && isset($_POST["nom"])  && isset($_POST["points"]) && isset($_POST["geo"]) && isset($_POST["id"]) && isset($_POST["zone"]) && isset($_POST["geo"]) ) {
    sqlexec("update keypoints set nom=".$conn->quote($_POST["nom"]).", points=".$conn->quote($_POST["points"]).", geo=".$conn->quote($_POST["geo"]).", zone_id=".$conn->quote($_POST["zone"])." where id=".$conn->quote($_POST["id"]));
    header('Location: map-editor.php');
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "add_keypoint" && isset($_POST["nom"])  && isset($_POST["points"]) && isset($_POST["geo"]) && isset($_POST["id"]) && isset($_POST["zone"]) && isset($_POST["geo"]) ) {
    sqlexec("insert into keypoints (nom,points,geo,zone_id) values (".$conn->quote($_POST["nom"]).",".$conn->quote($_POST["points"]).",".$conn->quote($_POST["geo"]).",".$conn->quote($_POST["zone"]).");"); 
    header('Location: map-editor.php');
    exit();
}

if (isset($_POST["action"]) && $_POST["action"] == "add_zone" && isset($_POST["nom"])  && isset($_POST["points"]) && isset($_POST["geo"]) && isset($_POST["zone"]) && isset($_POST["geo"]) ) {
    $voisins = json_decode($_POST["zone"]);
    $sqlquery = "insert into zones (nom,points,geo) values (".$conn->quote($_POST["nom"]).",".$conn->quote($_POST["points"]).",".$conn->quote(substr($_POST["geo"],1,-1))."); SELECT LAST_INSERT_ID() INTO @Last_Id;";
    foreach ($voisins as $voisin) {
        $sqlquery.="insert into frontieres values (@Last_Id,".$conn->quote($voisin).");";
    }
    sqlexec($sqlquery);
    header('Location: map-editor.php');
    exit();
}


$liste_zones=sqlexec("select id, nom, points, geo from zones order by nom");
$liste_keypoints=sqlexec("select keypoints.id as id, keypoints.nom as nom, keypoints.points as points, keypoints.geo as geo, zones.nom as zone from keypoints left join zones on keypoints.zone_id = zones.id order by nom");
$tab_zones = "";
$tab_keypoints = "";
$nb_zones = 0;
$nb_keypoints = 0;
$zones2JS = array();
$keypoints2JS = array();
if (count($liste_zones) > 0) {
    $tab_zones = "<table><thead><tr><th>Nom</th><th>Points</th><th>Nombre de frontières</th><th>Action</th></tr></thead><tbody>";
    foreach ($liste_zones as $zone){
        $id_voisins = [];
        foreach (sqlexec('select id_voisin from ( select id2 as id_voisin from frontieres where id1='.$zone["id"].' union select id1 as id_voisin from frontieres where id2='.$zone["id"].' ) as voisins') as $voisin) {
            array_push($id_voisins, $voisin["id_voisin"]);
        }
        $zones2JS[$zone["id"]] = array(
            "nom" => $zone["nom"],
            "points" => $zone["points"],
            "geo" => json_decode($zone["geo"]),
            "nb_frontiere" => count($id_voisins),
            "id_voisins" => $id_voisins
        );
        $nb_zones++;
        $tab_zones.="<tr>
        <td>".$zone["nom"]."</td>
        <td>".$zone["points"]."</td>
        <td>".count($id_voisins)."</td>
        <td><span class='button small' style='background-color: #00cf46;' data-toggle='modal' data-target='#modal_edit_map' onclick='edit_zone(\"".$zone["id"]."\");'>Editer</span> <span class='button small' style='background-color: #ff2800;' data-toggle='modal' data-target='#modal_delete_zone' onclick='delete_zone(\"".$zone["id"]."\",\"".$zone["nom"]."\");'>Supprimer</span></td>
        </tr>";
    }
    $tab_zones.="</tbody></table>";
}
if (count($liste_keypoints) > 0) {
    $tab_keypoints = "<table><thead><tr><th>Nom</th><th>Points</th><th>Zone associé</th><th>Action</th></tr></thead><tbody>";
    foreach ($liste_keypoints as $keypoint){
        $keypoints2JS[$keypoint["id"]] = array(
            "nom" => $keypoint["nom"],
            "points" => $keypoint["points"],
            "geo" => json_decode($keypoint["geo"]),
            "zone_name" => $keypoint["zone"]
        );
        $nb_keypoints++;
        $tab_keypoints.="<tr>
        <td>".$keypoint["nom"]."</td>
        <td>".$keypoint["points"]."</td>
        <td>".$keypoint["zone"]."</td>
        <td><span class='button small' style='background-color: #00cf46;' data-toggle='modal' data-target='#modal_edit_map' onclick='edit_keypoint(\"".$keypoint["id"]."\");'>Editer</span> <span class='button small' style='background-color: #ff2800;' data-toggle='modal' data-target='#modal_delete_keypoint' onclick='delete_keypoint(\"".$keypoint["id"]."\",\"".$keypoint["nom"]."\");'>Supprimer</span></td>
        </tr>";
    }
    $tab_keypoints.="</tbody></table>";
}
?>
<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Espace joueurs - Game of <?php echo $nom_ville; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <script src="js/jquery.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css" />
		<script src="js/bootstrap.min.js"></script>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
        <link rel="stylesheet" href="css/leaflet.css" />
        <script src="js/leaflet.js"></script>
        <script src="js/Leaflet.Editable.js"></script>
        <script src="js/Path.Drag.js"></script>
        <script src="js/wise-leaflet-pip.js"></script>
		<script>
var tile_server = '<?php echo $config["tile_server"] ?>';
var tile_attribution = '<?php echo $config["tile_attribution"] ?>';
var zones = JSON.parse('<?php echo str_replace("'", "\'", json_encode($zones2JS)) ?>');          
var keypoints = JSON.parse('<?php echo str_replace("'", "\'", json_encode($keypoints2JS)) ?>');          
var nb_zones = <?php echo $nb_zones; ?>;
        </script>
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header">
			<h1 id="header_site_name"><a href="admin.php">Game of <?php echo $nom_ville; ?></a></h1>
				<nav id="nav">
					<ul>
						<li><a href="admin.php#tous_les_duels">Duels</a></li>
						<li><a href="admin.php#liste_joueurs">Joueurs</a></li>
						<li><a href="admin.php#points cles">Points Clés</a></li>
						<li><a href="admin.php#zones">Zones</a></li>
						<li><a href="admin.php#config">Configuration</a></li>
                        <li><a href="map-editor.php">Editer la carte</a></li>
						<li><a href="admin.php#reset_game">Réinitialisation</a></li>
						<li><a href="mon-compte.php">Mon Compte</a></li>
						<li><a href="logout.php" class="button special">Déconnexion</a></li>
					</ul>
				</nav>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h2 style="margin:0;">Bonjour, <?php echo $_SESSION["nom"]; ?></h2>
				<p>Edition de la carte</p>
			</section>

        <!-- Two -->
			<section id="tile_server" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2>Serveur de tuiles</h2>
						<p>Modifier le serveur de tuiles OSM utilisé pour afficher le fond de la carte</p>
					</header>
				</div>
				<div id="formulaires_tile_server">
					<div id="form_update_tile" class="container 50%">
						<form id="update_tile" action="#" method="post">
							<input name="action" value="update_tile" type="hidden">
							<div class="row uniform">
								<div class="row uniform">
									<p>URL du serveur (privilégier https):</p>
								</div>
								<div class="12u">
                                    <input type="text" value="<?php echo $config["tile_server"]; ?>" name="server" />
								</div>
                                <div class="row uniform">
									<p>Mention légale à afficher:</p>
								</div>
								<div class="12u">
                                    <input type="text" value="<?php echo $config["tile_attribution"]; ?>" name="attribution" />
								</div>
								<div class="12u$">
									<ul class="actions">
										<li><input value="Mettre a jour l'URL" class="special" type="submit"></li>
									</ul>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>
        <!-- Three -->
            <section id="zones" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2><?php echo $nb_zones; ?> Zones</h2>
					</header>
					<div class="row" id="liste_zones">
                        <div class="12u$" style="margin-bottom : 20px;">
                                <span class="button" data-toggle='modal' data-target='#modal_edit_map' onclick='add_zone();'>Ajouter une zone</span>
                        </div>
                        <?php echo $tab_zones; ?>
					</div>
				</div>
			</section>
			
        <!-- Four -->
			<section id="points cles" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2><?php echo $nb_keypoints; ?> Points clés</h2>
					</header>
					<div class="row" id="liste_pt_cles">
                        <div class="12u$" style="margin-bottom : 20px;">
                                <span class="button" data-toggle='modal' data-target='#modal_edit_map' onclick='add_keypoint();'>Ajouter un point clé</span>
                        </div>
                        <?php echo $tab_keypoints; ?>
					</div>
				</div>
			</section>
		<?php echo $footer; ?>

<div class="modal fade" id="modal_delete_zone" tabindex="-1" role="dialog" aria-labelledby="modal_delete_zone" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Supprimer une zone</h5>
            </div>
            <div class="modal-body">
                Souhaitez vous supprimer la zone "<span id="delete_zone_name"></span>"?</br>Cela peut altérer le fonctionnement du jeu si une partie est en cours 
            </div>
            <div class="modal-footer">
                <form method="post" action="#" id="form_delete_zone">
                    <input name="action" value="delete_zone" type="hidden">
                    <input id="id_delete_zone" name="id" value="" type="hidden">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="document.getElementById('form_delete_zone').submit();">Supprimer cette zone</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_delete_keypoint" tabindex="-1" role="dialog" aria-labelledby="modal_delete_keypoint" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Supprimer un point clé</h5>
            </div>
            <div class="modal-body">
                Souhaitez vous supprimer le point clé "<span id="delete_keypoint_name"></span>"?</br>Cela peut altérer le fonctionnement du jeu si une partie est en cours 
            </div>
            <div class="modal-footer">
                <form method="post" action="#" id="form_delete_keypoint">
                    <input name="action" value="delete_keypoint" type="hidden">
                    <input id="id_delete_keypoint" name="id" value="" type="hidden">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="document.getElementById('form_delete_keypoint').submit();">Supprimer ce point clé</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="modal_edit_map" tabindex="-1" role="dialog" aria-labelledby="modal_edit_map" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="post" action="#" id="form_edit_map">
                <div class="modal-header">
                    <h5 class="modal-title" id="edit_map_title"><span id="map_action"></span> <span id="map_type"></span> <span id="map_nom"></span></h5>
                </div>
                <div class="modal-body">
                    <label>Nom : <input type="text" id="input_nom" name="nom" /></label>
                    <label>Points : <input type="number" style="width:5em;" id="input_points" name="points" /></label>
                </div>
                <div id="map_container" class="row container" style="margin:auto;padding:0;width:998px;height:600px;">
                    <div id="map" ></div>
                </div>
                <div class="modal-footer">
                    <input id="php_map_action" name="action" value="" type="hidden">
                    <input id="input_geo" name="geo" value="" type="hidden">
                    <input id="input_id" name="id" value="" type="hidden">
                    <input id="input_zone_id" name="zone" value="" type="hidden">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" id="BtnMapEnregistrer" class="btn btn-primary" data-dismiss="modal" onclick="document.getElementById('form_edit_map').submit();">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
function delete_zone(id,nom) {
    document.getElementById("delete_zone_name").innerHTML = nom;
    document.getElementById("id_delete_zone").value = id;
}            
function delete_keypoint(id,nom) {
    document.getElementById("delete_keypoint_name").innerHTML = nom;
    document.getElementById("id_delete_keypoint").value = id;
}
    
function submit_edit_zone(id){
    if (document.getElementById("input_nom") == "") { return false; }
    document.getElementById("php_map_action").value = "edit_zone";
    document.getElementById("input_geo").value = JSON.stringify(L.GeoJSON.latLngsToCoords(zones_on_map._layers[zones_on_map._leaflet_id+1+id]._latlngs,1));
    document.getElementById("input_id").value = id;    
    document.getElementById("input_zone_id").value = JSON.stringify(zones[id].id_voisins);
    document.getElementById('form_edit_map').submit();
    return true;
}    
function submit_add_zone(){
    if (document.getElementById("input_nom") == "") { return false; }
    document.getElementById("php_map_action").value = "add_zone";
    document.getElementById("input_geo").value = JSON.stringify(L.GeoJSON.latLngsToCoords(new_zone._latlngs,1));
    document.getElementById("input_zone_id").value = JSON.stringify(new_zone.id_voisins);
    document.getElementById('form_edit_map').submit();
    return true;
}    
function submit_edit_keypoint(id){
    if (document.getElementById("input_nom") == "") { return false; }
    detect_zone_from_marker(new_keypoint._latlng);
    document.getElementById("php_map_action").value = "edit_keypoint";
    document.getElementById("input_geo").value = JSON.stringify(L.GeoJSON.latLngToCoords(new_keypoint._latlng));
    document.getElementById("input_id").value = id; 
    document.getElementById("input_zone_id").value = zone_id;
    document.getElementById('form_edit_map').submit();
    return true;
}    
function submit_add_keypoint(){
    if (document.getElementById("input_nom") == "") { return false; }
    detect_zone_from_marker(new_keypoint._latlng);
    document.getElementById("php_map_action").value = "add_keypoint";
    document.getElementById("input_geo").value = JSON.stringify(L.GeoJSON.latLngToCoords(new_keypoint._latlng));
    document.getElementById("input_zone_id").value = zone_id;
    document.getElementById('form_edit_map').submit();
    return true;
}

function toggle_voisin(id, id_voisin){
    if (id == "new_zone") {
        if (new_zone.id_voisins.indexOf(id_voisin) != "-1") {
            document.getElementById("BtnVoisin"+id_voisin).innerHTML = "Est voisin";
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)]._popup._content = document.getElementById("popup"+id_voisin).outerHTML;
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)].setStyle({color: "#000000", weight: "1", fillColor: "#000000", fillOpacity: "0.7"});
            new_zone.id_voisins.splice(new_zone.id_voisins.indexOf(id_voisin),1);
        } else {
            document.getElementById("BtnVoisin"+id_voisin).innerHTML = "N'est pas voisin";
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)]._popup._content = document.getElementById("popup"+id_voisin).outerHTML;
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)].setStyle({color: "#000000", weight: "1", fillColor: "#ff0000", fillOpacity: "0.7"});
            new_zone.id_voisins.push(id_voisin);
        }
    } else {
        if (zones[id].id_voisins.indexOf(id_voisin) != "-1") {
            document.getElementById("BtnVoisin"+id_voisin).innerHTML = "Est voisin";
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)]._popup._content = document.getElementById("popup"+id_voisin).outerHTML;
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)].setStyle({color: "#000000", weight: "1", fillColor: "#000000", fillOpacity: "0.7"});
            zones[id].id_voisins.splice(zones[id].id_voisins.indexOf(id_voisin),1);
        } else {
            document.getElementById("BtnVoisin"+id_voisin).innerHTML = "N'est pas voisin";
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)]._popup._content = document.getElementById("popup"+id_voisin).outerHTML;
            zones_on_map._layers[zones_on_map._leaflet_id+1+parseInt(id_voisin)].setStyle({color: "#000000", weight: "1", fillColor: "#ff0000", fillOpacity: "0.7"});
            zones[id].id_voisins.push(id_voisin);
        }
    }
}
    
function detect_zone_from_marker(latlng) {
    var zone = 0;
    for (var i=1; i <= nb_zones; i++) {
        if (zones_on_map._layers[zones_on_map._leaflet_id+1+i].contains(latlng)) {
            zone = i;
        }
    }
    zone_id = zone;
}
//map
var zones_on_map;
var keypoints_on_map;
var map;
var new_zone;
var new_keypoint;
var zone_id;
    
function edit_zone(id){
    document.getElementById("BtnMapEnregistrer").setAttribute('onclick', 'submit_edit_zone('+id+')');
    document.getElementById("map_container").innerHTML = '<div id="map" ></div>';
    document.getElementById("map_action").innerHTML = "Editer";
    document.getElementById("map_type").innerHTML = "la zone";
    document.getElementById("map_nom").innerHTML = zones[id]["nom"];
    document.getElementById("input_nom").value = zones[id]["nom"];
    var select_points = "<select id='input_points' name='points'>";
    for (var i = 1; i<=5; i++) {
        if (i == zones[id]["points"]) {
            select_points+="<option value='"+i+"' selected>"+i+" Point(s)</option>";
        } else {
            select_points+="<option value='"+i+"'>"+i+" Point(s)</option>";
        }
    }
    select_points+="</select>";
    document.getElementById("input_points").outerHTML = select_points;
    zones_on_map = null;
    keypoints_on_map = null;
    map = null;
    function onEachFeature(feature, layer, type) {

		if (type == "zone" && zones[id]["id_voisins"].indexOf(feature.properties.id) == "-1" && feature.properties.id != id) {
            layer.bindPopup('<span id="popup'+feature.properties.id+'"><h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+ '</br></br> <p id="BtnVoisin'+feature.properties.id+'" class="button small" style="margin:auto;display:block;" onclick="toggle_voisin(\''+id+'\',\''+feature.properties.id+'\');">Est voisin</p></span>');
            layer.setStyle({color: "#000000", weight: "1", fillColor: "#000000", fillOpacity: "0.7"});
        } else if (type == "zone" && zones[id]["id_voisins"].indexOf(feature.properties.id) != "-1"){
            layer.bindPopup('<span id="popup'+feature.properties.id+'"><h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+ '</br></br> <p id="BtnVoisin'+feature.properties.id+'" class="button small" style="margin:auto;display:block;" onclick="toggle_voisin(\''+id+'\',\''+feature.properties.id+'\');">N\'est pas voisin</p></span>');
            layer.setStyle({color: "#000000", weight: "1", fillColor: "#ff0000", fillOpacity: "0.7"});
        } else if (type == "zone" && feature.properties.id == id){
            layer.setStyle({color: "#000000", weight: "1", fillColor: "#0000ff", fillOpacity: "0.7"});
        } else {
            layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points);
            layer.setStyle({color: "#000000", weight: "1", fillColor: "#000000", fillOpacity: "0.7"});
        }
        
    }
    function getTrace(){
     $.ajax({
               type: "POST",
                url: "php/getTrace.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        var layer = L.polygon(L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates));
                        onEachFeature(feature, layer, "zone");
                        layer.addTo(zones_on_map);
                    })
                }
    setTimeout(getMark, 50);		
    }

    function getMark(){
     $.ajax({
               type: "POST",
                url: "php/getMark.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        var layer = L.circleMarker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates), {radius:8});
                        onEachFeature(feature, layer, "keypoint");
                        layer.addTo(keypoints_on_map);
                    })
                }
    setTimeout('calibrage_map('+id+')', 1000);
    }
    map = L.map('map', {editable: true});
    //url des TMS de fond
    var osm = L.tileLayer(tile_server, {attribution: tile_attribution,
        minZoom: 1,
        maxZoom: 18});

    //on ajoute le fond OSM au départ
    osm.addTo(map)
    //on ajoute le 'control' des fonds de carte

    zones_on_map = L.featureGroup().addTo(map);
    keypoints_on_map = L.featureGroup().addTo(map);
    getTrace();
    setTimeout('zones_on_map._layers[zones_on_map._leaflet_id+1+'+id+'].enableEdit()', 2000);
}    
function add_zone(){
    document.getElementById("BtnMapEnregistrer").setAttribute('onclick', 'submit_add_zone()');
    document.getElementById("map_container").innerHTML = '<div id="map" ></div>';
    document.getElementById("map_action").innerHTML = "Ajouter";
    document.getElementById("map_type").innerHTML = "une zone";
    var select_points = "<select id='input_points' name='points'>";
    for (var i = 1; i<=5; i++) {
        select_points+="<option value='"+i+"'>"+i+" Point(s)</option>";
    }
    select_points+="</select>";
    document.getElementById("input_points").outerHTML = select_points;
    zones_on_map = null;
    keypoints_on_map = null;
    map = null;
    function onEachFeature(feature, layer, type) {
        layer.bindPopup('<span id="popup'+feature.properties.id+'"><h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points+ '</br></br> <p id="BtnVoisin'+feature.properties.id+'" class="button small" style="margin:auto;display:block;" onclick="toggle_voisin(\'new_zone\',\''+feature.properties.id+'\');">Est voisin</p></span>');
            layer.setStyle({color: "#000000", weight: "1", fillColor: "#000000", fillOpacity: "0.7"});
    }
    
    function getTrace(){
     $.ajax({
               type: "POST",
                url: "php/getTrace.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        var layer = L.polygon(L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates));
                        onEachFeature(feature, layer, "zone");
                        layer.addTo(zones_on_map);
                    })
                }
    setTimeout(getMark, 50);		
    }

    function getMark(){
     $.ajax({
               type: "POST",
                url: "php/getMark.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        var layer = L.circleMarker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates), {radius:8});
                        onEachFeature(feature, layer, "keypoint");
                        layer.addTo(keypoints_on_map);
                    })
                }
    setTimeout(calibrage_map, 1000);
    }
    map = L.map('map', {editable: true});
    //url des TMS de fond
    var osm = L.tileLayer(tile_server, {attribution: tile_attribution,
        minZoom: 1,
        maxZoom: 18});

    //on ajoute le fond OSM au départ
    osm.addTo(map)
    //on ajoute le 'control' des fonds de carte

    zones_on_map = L.featureGroup().addTo(map);
    keypoints_on_map = L.featureGroup().addTo(map);
    getTrace();
    setTimeout('new_zone = map.editTools.startPolygon(); new_zone.id_voisins = [];', 2000);
}    
    
function edit_keypoint(id){
    document.getElementById("BtnMapEnregistrer").setAttribute('onclick', 'submit_edit_keypoint('+id+')');
    document.getElementById("map_container").innerHTML = '<div id="map" ></div>';
    document.getElementById("map_action").innerHTML = "Editer";
    document.getElementById("map_type").innerHTML = "le point clé";
    document.getElementById("map_nom").innerHTML = keypoints[id]["nom"];
    document.getElementById("input_nom").value = keypoints[id]["nom"];
    var select_points = "<select id='input_points' name='points'>";
    for (var i = 1; i<=5; i++) {
        if (i == keypoints[id]["points"]) {
            select_points+="<option value='"+i+"' selected>"+i+" Point(s)</option>";
        } else {
            select_points+="<option value='"+i+"'>"+i+" Point(s)</option>";
        }
    }
    select_points+="</select>";
    document.getElementById("input_points").outerHTML = select_points;
    zones_on_map = null;
    keypoints_on_map = null;
    map = null;
    
    function onEachFeature(feature, layer) {
        layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points);
        layer.setStyle({color: "#000000", weight: "1", fillColor: "#000000", fillOpacity: "0.7"});
    }
        

    function getTrace(){
     $.ajax({
               type: "POST",
                url: "php/getTrace.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        var layer = L.polygon(L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates));
                        onEachFeature(feature, layer);
                        layer.addTo(zones_on_map);
                    })
                }
    setTimeout(getMark, 50);		
    }

    function getMark(){
     $.ajax({
               type: "POST",
                url: "php/getMark.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        if (feature.properties.id == id) {
                            new_keypoint = L.marker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates));
                            new_keypoint.addTo(map);
                            detect_zone_from_marker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates));
                        } else {
                            var layer = L.circleMarker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates), {radius:8});
                            onEachFeature(feature, layer);
                            layer.addTo(keypoints_on_map);
                        }
                    })
                }    
    setTimeout('calibrage_map(zone_id)', 1000);
    }
    map = L.map('map', {editable: true});
    //url des TMS de fond
    var osm = L.tileLayer(tile_server, {attribution: tile_attribution,
        minZoom: 1,
        maxZoom: 18});

    //on ajoute le fond OSM au départ
    osm.addTo(map)
    //on ajoute le 'control' des fonds de carte

    zones_on_map = L.featureGroup().addTo(map);
    keypoints_on_map = L.featureGroup().addTo(map);
    getTrace();
    setTimeout('new_keypoint.enableEdit()', 2000);
}    
function add_keypoint(){
    document.getElementById("BtnMapEnregistrer").setAttribute('onclick', 'submit_add_keypoint()');
    document.getElementById("map_container").innerHTML = '<div id="map" ></div>';
    document.getElementById("map_action").innerHTML = "Ajouter";
    document.getElementById("map_type").innerHTML = "un point clé";
    var select_points = "<select id='input_points' name='points'>";
    for (var i = 1; i<=5; i++) {
        select_points+="<option value='"+i+"'>"+i+" Point(s)</option>";
    }
    select_points+="</select>";
    document.getElementById("input_points").outerHTML = select_points;
    zones_on_map = null;
    keypoints_on_map = null;
    map = null;
    
    function onEachFeature(feature, layer) {
        layer.bindPopup('<h1 style="text-align : center;">' + feature.properties.Nom +'</h1>Points : ' +feature.properties.points);
        layer.setStyle({color: "#000000", weight: "1", fillColor: "#000000", fillOpacity: "0.7"});
    }
        

    function getTrace(){
     $.ajax({
               type: "POST",
                url: "php/getTrace.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        var layer = L.polygon(L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates));
                        onEachFeature(feature, layer);
                        layer.addTo(zones_on_map);
                    })
                }
    setTimeout(getMark, 50);		
    }

    function getMark(){
     $.ajax({
               type: "POST",
                url: "php/getMark.php",
                success: result,
                dataType: 'json'
                });

                function result(data){
                    data.features.forEach(function(feature){
                        var layer = L.circleMarker(L.GeoJSON.coordsToLatLng(feature.geometry.coordinates), {radius:8});
                        onEachFeature(feature, layer);
                        layer.addTo(keypoints_on_map);
                    })
                }    
    setTimeout('calibrage_map()', 1000);
    }
    map = L.map('map', {editable: true});
    //url des TMS de fond
    var osm = L.tileLayer(tile_server, {attribution: tile_attribution,
        minZoom: 1,
        maxZoom: 18});

    //on ajoute le fond OSM au départ
    osm.addTo(map)
    //on ajoute le 'control' des fonds de carte

    zones_on_map = L.featureGroup().addTo(map);
    keypoints_on_map = L.featureGroup().addTo(map);
    getTrace();
    setTimeout('new_keypoint = L.marker(map.getCenter()); new_keypoint.addTo(map);', 1500);
    setTimeout('new_keypoint.enableEdit()', 2000);
}
    



function calibrage_map(zone = false) {
    if (zone !== false) {
        map.fitBounds(zones_on_map._layers[zones_on_map._leaflet_id+1+zone].getBounds());
    } else if (zones_on_map.getLayers().length == 0) {
        map.setView([45,4],5);
    } else {
        map.fitBounds(zones_on_map.getBounds());
    }
}
      

</script>
	</body>
</html>