<?php
session_start();
include('php/config.php');
actualiser_session();

if ($_SESSION["id"] != 0) {
	header('Location: espace-joueurs.php');
	exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_penalites" && isset($_POST["valeur"]) ) {
    if ($_POST["valeur"] == "") { $_POST["valeur"] = 'null'; } else { $_POST["valeur"] = "'".$_POST["valeur"]."'"; }
    sqlexec("update config set litige_penalite = $_POST[valeur]");
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_jours_revendication" && isset($_POST["valeur"]) ) {
    if ($_POST["valeur"] == "") { $_POST["valeur"] = 'null'; } else { $_POST["valeur"] = "'".$_POST["valeur"]."'"; }
    sqlexec("update config set jours_revendication = $_POST[valeur]");
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_config" && isset($_POST["champ"]) && isset($_POST["valeur"]) ) {
    $datenow = new DateTime('NOW');
    if ($_POST["valeur"] == "") { $_POST["valeur"] = 'null'; $newdate = new DateTime('NOW'); $newdate->modify('+1 day'); } else { $newdate = new DateTime($_POST["valeur"]);$_POST["valeur"] = "'".$_POST["valeur"]."'"; }
    if ($_POST["champ"] != "date_reset" || $newdate > $datenow) {
        sqlexec("update config set $_POST[champ] = $_POST[valeur]");
    }
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_int" && isset($_POST["champ"]) && isset($_POST["valeur"]) ) {
    sqlexec("update config set $_POST[champ] = $_POST[valeur]");
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_penalite" && isset($_POST["id"]) && is_numeric($_POST["id"]) && isset($_POST["penalite"]) && is_numeric($_POST["penalite"])) {
    sqlexec("update joueurs set point_penalite=$_POST[penalite] where id=".$_POST["id"]);
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "calcul_penalite" && isset($_POST["id"]) && is_numeric($_POST["id"])) {
    $penalites_a_appliquer = sqlexec("select if(duels.zone_id,'zones','keypoints') as type_territoire, if(duels.zone_id,zone_id,keypoint_id) as id_territoire from duels where status = 'END' and litige=1 and gagnant=0 and attaquant=$_POST[id]");
    $pt_penalite=0;
    foreach($penalites_a_appliquer as $penalite) {
        $pt_penalite = $pt_penalite + intval(sqlexec("select points from $penalite[type_territoire] where id = $penalite[id_territoire];")[0]["points"]);
    }
    sqlexec("update joueurs set point_penalite=$pt_penalite where id=$_POST[id]");
    echo json_encode($pt_penalite);
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "reset_password" && isset($_POST["id"]) && is_numeric($_POST["id"])) {
    sqlexec("update joueurs set password='cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e' where id=".$_POST["id"]);
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "delete_user" && isset($_POST["id"]) && is_numeric($_POST["id"])) {
    sqlexec("delete from joueurs where id=".$_POST["id"]." and id != 0;");
    header('Location: admin.php');
	exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_zone" && isset($_POST["id"]) && is_numeric($_POST["id"]) && isset($_POST["points"]) && is_numeric($_POST["points"]) && isset($_POST["proprietaire"]) && is_numeric($_POST["proprietaire"])) {
    sqlexec("update zones set points=".$_POST["points"].", proprietaire=".$_POST["proprietaire"]." where id=".$_POST["id"].";");
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "update_keypoint" && isset($_POST["id"]) && is_numeric($_POST["id"]) && isset($_POST["points"]) && is_numeric($_POST["points"]) && isset($_POST["proprietaire"]) && is_numeric($_POST["proprietaire"])) {
    sqlexec("update keypoints set points=".$_POST["points"].", proprietaire=".$_POST["proprietaire"]." where id=".$_POST["id"].";");
    exit();
}
if (isset($_POST["action"]) && $_POST["action"] == "reset_game") {
    reset_game();
    header('Location: admin.php');
    exit();
}


$liste_points = array ("1","2","3","4","5");
$liste_joueurs=sqlexec("select id, nom, couleur, nom_couleur, point_penalite, IFNULL(DATE_FORMAT(last_login, '%d/%m/%Y à %T'), 'Jamais') as last_login, IFNULL(DATE_FORMAT(last_revendication, '%d/%m/%Y à %T'), 'Jamais') as last_revendication from joueurs where id > 0 order by nom");
$liste_zones=sqlexec("select id, nom, points, proprietaire, IFNULL(duel_id,'Non') as duel_id from zones order by nom");
$liste_keypoints=sqlexec("select id, nom, points, proprietaire, IFNULL(duel_id,'Non') as duel_id from keypoints order by nom");
$tab_zones = "";
$tab_keypoints = "";
$tab_joueurs = "";
$nb_joueurs = 0;
$nb_zones = 0;
$nb_keypoints = 0;
if (count($liste_joueurs) > 0) {
    $tab_joueurs = "<table><thead><tr><th></th><th>Nom</th><th>Dernière connexion</th><th>Dernière revendication</th><th>Penalités</th><th>Actions</th></tr></thead><tbody>";
    foreach ($liste_joueurs as $joueur){
        $nb_joueurs++;
        $tab_joueurs.="<tr><td><b style=\"display: block; width: 20px; height: 20px; background-color: ".$joueur["couleur"]."; margin:auto; border-radius:50%; border: 1px solid;\"></b></td><td>".$joueur["nom"]."</td><td>".$joueur["last_login"]."</td><td>".$joueur["last_revendication"]."</td><td><input id='penalite_$joueur[id]' type='number' value='$joueur[point_penalite]' style='width:3em;' onblur='update_penalite($joueur[id]);'></td><td><span class='button small' onclick='calcul_penalite(\"".$joueur["id"]."\");'>Calculer les pénalités</span> <span class='button small' style='background-color: #00cf46;' data-toggle='modal' data-target='#modal_reset' onclick='reset_password(\"".$joueur["id"]."\",\"".$joueur["nom"]."\");'>Réinitialiser le mot de passe</span> <span class='button small' style='background-color: #ff2800;' data-toggle='modal' data-target='#modal_delete' onclick='delete_user(\"".$joueur["id"]."\",\"".$joueur["nom"]."\");'>Supprimer le joueur</span></td></tr>";
    }
    $tab_joueurs.="</tbody></table>";
}
if (count($liste_zones) > 0) {
    $tab_zones = "<table><thead><tr><th>Nom</th><th>Points</th><th>Propriétaire</th><th>Duel en cours</th></tr></thead><tbody>";
    foreach ($liste_zones as $zone){
        $nb_zones++;
        $select_points = "<select id='points_zone_$zone[id]' onchange='updateTerritoire(\"zone\",$zone[id])'>";
        $select_proprietaire = "<select id='proprietaire_zone_$zone[id]' onchange='updateTerritoire(\"zone\",$zone[id])'><option value='0'>Aucun proprietaire</option>";
        foreach ($liste_points as $points){
            if ($points == $zone["points"]) {
                $select_points.="<option value='".$points."' selected>".$points." Point(s)</option>";
            } else {
                $select_points.="<option value='".$points."'>".$points." Point(s)</option>";
            }
        }
        foreach ($liste_joueurs as $joueur){
            if ($joueur["id"] == $zone["proprietaire"]) {
                $select_proprietaire.="<option value='".$joueur["id"]."' selected>".$joueur["nom"]."</option>";
            } else {
                $select_proprietaire.="<option value='".$joueur["id"]."'>".$joueur["nom"]."</option>";
            }
        }
        $select_points.="</select>";
        $select_proprietaire.="</select>";
        if ($zone["duel_id"] == "Non") {
            $duel_en_cours = "Non";
        } else {
            $duel_en_cours = "<a href='duel.php?id=".$zone["duel_id"]."'>Duel n°".$zone["duel_id"]."</a>";
        }
        $tab_zones.="<tr>
        <td>".$zone["nom"]."</td>
        <td>".$select_points."</td>
        <td>".$select_proprietaire."</td>
        <td>".$duel_en_cours."</td>
        </tr>";
    }
    $tab_zones.="</tbody></table>";
}
if (count($liste_keypoints) > 0) {
    $tab_keypoints = "<table><thead><tr><th>Nom</th><th>Points</th><th>Propriétaire</th><th>Duel en cours</th></tr></thead><tbody>";
    foreach ($liste_keypoints as $keypoint){
        $nb_keypoints++;
        $select_points = "<select id='points_keypoint_$keypoint[id]' onchange='updateTerritoire(\"keypoint\",$keypoint[id])'>";
        $select_proprietaire = "<select id='proprietaire_keypoint_$keypoint[id]' onchange='updateTerritoire(\"keypoint\",$keypoint[id])'><option value='0'>Aucun proprietaire</option>";
        foreach ($liste_points as $points){
            if ($points == $keypoint["points"]) {
                $select_points.="<option value='".$points."' selected>".$points." Point(s)</option>";
            } else {
                $select_points.="<option value='".$points."'>".$points." Point(s)</option>";
            }
        }
        foreach ($liste_joueurs as $joueur){
            if ($joueur["id"] == $keypoint["proprietaire"]) {
                $select_proprietaire.="<option value='".$joueur["id"]."' selected>".$joueur["nom"]."</option>";
            } else {
                $select_proprietaire.="<option value='".$joueur["id"]."'>".$joueur["nom"]."</option>";
            }
        }
        $select_points.="</select>";
        $select_proprietaire.="</select>";
        if ($keypoint["duel_id"] == "Non") {
            $duel_en_cours = "Non";
        } else {
            $duel_en_cours = "<a href='duel.php?id=".$keypoint["duel_id"]."'>Duel n°".$keypoint["duel_id"]."</a>";
        }
        $tab_keypoints.="<tr>
        <td>".$keypoint["nom"]."</td>
        <td>".$select_points."</td>
        <td>".$select_proprietaire."</td>
        <td>".$duel_en_cours."</td>
        </tr>";
    }
    $tab_keypoints.="</tbody></table>";
}
?>
<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Espace joueurs - Game of <?php echo $nom_ville; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <script src="js/jquery.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css" />
		<script src="js/bootstrap.min.js"></script>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<script>
// liste tous les duels en cours
function getDuels(){
	$.ajax({
	   type: "POST",
		url: "php/getDuels.php",
		success: result,
		dataType: "json"
		});

		function result(data){
			if (data.length > 0) {
				compteur=0;
				tab_Duels_envoyes = "<table><thead><tr><th>Date</th><th>Nom de l'attaquant</th><th>Nom du défenseur</th><th>Territoire</th></tr></thead><tbody>";
				data.forEach(function (duel){
					compteur++;
                    if (duel.litige == "1") {
					   tab_Duels_envoyes += "<tr onclick=\"document.location='duel.php?id="+duel.id+"'\" style='cursor:pointer;background-color: #A52A2A;color:white;'><td>"+duel.update_config+"</td><td>"+duel.attaquant_nom+"</td><td>"+duel.defenseur_nom+"</td><td>"+duel.type_territoire+" "+duel.territoire_nom+" ("+duel.territoire_points+" Points)</td></tr>";
                    } else {
                        tab_Duels_envoyes += "<tr onclick=\"document.location='duel.php?id="+duel.id+"'\" style='cursor:pointer;'><td>"+duel.update_config+"</td><td>"+duel.attaquant_nom+"</td><td>"+duel.defenseur_nom+"</td><td>"+duel.type_territoire+" "+duel.territoire_nom+" ("+duel.territoire_points+" Points)</td></tr>";
                    }
				})
				tab_Duels_envoyes += "</tbody></table>";
				document.getElementById("duels").innerHTML = tab_Duels_envoyes;
				if (compteur == 1){
					document.getElementById("nbDuels").innerHTML = compteur+" Duel en cours";
				} else {
					document.getElementById("nbDuels").innerHTML = compteur+" Duels en cours";
				}
			} else {
				document.getElementById("duels").innerHTML = "";
			}
		}
}
getDuels();
var refresh_Duels = setInterval(getDuels, 60000);
            
function reset_password(id,nom) {
    document.getElementById("reset_name").innerHTML = nom;
    document.getElementById("validate_reset_password").onclick = function(){validate_reset_password(id);};
}            
function delete_user(id,nom) {
    document.getElementById("delete_name").innerHTML = nom;
    document.getElementById("id_delete_user").value = id;
}

function updateTerritoire(type,id){
    points = document.getElementById("points_"+type+"_"+id).value;
    proprietaire = document.getElementById("proprietaire_"+type+"_"+id).value;
    
	$.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=update_"+type+"&id="+id+"&points="+points+"&proprietaire="+proprietaire,
		dataType: "html"
	});
}
function validate_reset_password(id){    
	$.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=reset_password&id="+id,
		dataType: "html"
	});
}
function update_penalite(id){
    penalite = document.getElementById("penalite_"+id).value;
	$.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=update_penalite&id="+id+"&penalite="+penalite,
		dataType: "html"
	});
}
function calcul_penalite(id){
	$.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=calcul_penalite&id="+id,
        success: result,
		dataType: "json"
	});
    
    function result(data){
        document.getElementById("penalite_"+id).value = data;
    }
}
            
function update_config(champ) {
    valeur = document.getElementById(champ).value;
    if (champ == "date_reset") {
        datemin = new Date(document.getElementById("date_reset").min);
        if (valeur == "") { date = datemin; } else {date = new Date(valeur);}
        if (date < datemin) {
            document.getElementById("date_reset").value = "";
            update_config("date_reset");
        }
    } else if (!champ.match(/date.*/)) {
        min = 1;
        if (valeur < min) {
            document.getElementById(champ).value = 1;
            update_config(champ);
        }
    }
    
    $.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=update_config&champ="+champ+"&valeur="+valeur,
		dataType: "html"
	});
}            
function update_int(champ) {
    valeur = document.getElementById(champ).value;
    
    $.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=update_int&champ="+champ+"&valeur="+valeur,
		dataType: "html"
	});
}           
function update_jours_revendications() {
    jours_revendication = ",";
    $('input[name=jours_semaine]').each(function() {
        if ($(this)[0].checked)  {
            jours_revendication = jours_revendication+","+$(this)[0].value;
        }
    });
    jours_revendication = jours_revendication.substr(2);
    $.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=update_jours_revendication&valeur="+jours_revendication,
		dataType: "html"
	});
}           
function update_penalites() {
    if (document.getElementById("penalite").checked) {
        litige_penalite = "1";
    } else {
        litige_penalite = "0";
    }
    $.ajax({
		type: "POST",
		url: "admin.php",
        data: "action=update_penalites&valeur="+litige_penalite,
		dataType: "html"
	});
}
		</script>
		
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header">
			<h1 id="header_site_name"><a href="index.php">Game of <?php echo $nom_ville; ?></a></h1>
				<nav id="nav">
					<ul>
						<li><a href="admin.php#tous_les_duels">Duels</a></li>
						<li><a href="admin.php#liste_joueurs">Joueurs</a></li>
						<li><a href="admin.php#points cles">Points Clés</a></li>
						<li><a href="admin.php#zones">Zones</a></li>
						<li><a href="admin.php#config">Configuration</a></li>
                        <li><a href="map-editor.php">Editer la carte</a></li>
                        <li><a href="admin.php#reset_game">Réinitialisation</a></li>
						<li><a href="mon-compte.php">Mon Compte</a></li>
						<li><a href="logout.php" class="button special">Déconnexion</a></li>
					</ul>
				</nav>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h2 style="margin:0;">Bonjour, <?php echo $_SESSION["nom"]; ?></h2>
				<p>Espace d'administration du jeu</p>
			</section>

		<!-- One -->
			<section id="tous_les_duels" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Tous les duels en cours</h2>
						<p>Les lignes en rouge sont les duels en litige</p>
						<p id="nbDuels">Aucun duel en cours</p>
					</header>
					<div class="row" id="duels">
					</div>
				</div>
			</section>
        <!-- Two -->
			<section id="liste_joueurs" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2><?php echo $nb_joueurs; ?> Joueurs</h2>
					</header>
					<div class="row" id="liste_joueurs">
                        <?php echo $tab_joueurs; ?>
					</div>
				</div>
			</section>
        <!-- Three -->
            <section id="zones" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2><?php echo $nb_zones; ?> Zones</h2>
					</header>
					<div class="row" id="liste_zones">
                        <?php echo $tab_zones; ?>
					</div>
				</div>
			</section>
			
        <!-- Four -->
			<section id="points cles" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2><?php echo $nb_keypoints; ?> Points clés</h2>
					</header>
					<div class="row" id="liste_pt_cles">
                        <?php echo $tab_keypoints; ?>
					</div>
				</div>
			</section>
        <!-- Five -->
			<section id="config" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Configuration du jeu</h2>
					</header>
					<div class="row 150%">
						<div class="4u 12u$(medium)">
							<section class="box">
								<h3>Dates des phases du jeu</h3>
								<ul class="alt">
                                    <li><label>Date de réinitialisation automatique</br>
<input id="date_reset" onblur="update_config('date_reset');" type="date" value="<?php if ($config["date_reset"] != null) {echo date('Y-m-d', strtotime($config["date_reset"]));}?>" min= "<?php $date = new DateTime('NOW');
$date->modify('+1 day');
echo $date->format('Y-m-d'); ?>"/></label></li>
									<li><label>Date du début de la phase éliminatoire</br>
<input id="date_eliminatoires" onblur="update_config('date_eliminatoires');" type="date" value="<?php if ($config["date_eliminatoires"] != null) {echo date('Y-m-d', strtotime($config["date_eliminatoires"]));}?>" /></label></li>
									<li><label>Date du début des inscriptions</br>
<input id="date_inscription" onblur="update_config('date_inscription');" type="date" value="<?php if ($config["date_inscription"] != null) {echo date('Y-m-d', strtotime($config["date_inscription"]));}?>" /></label></li>
									<li><label>Date de fin des inscriptions</br>
<input id="date_fin_inscription" onblur="update_config('date_fin_inscription');" type="date" value="<?php if ($config["date_fin_inscription"] != null) {echo date('Y-m-d', strtotime($config["date_fin_inscription"]));}?>" /></label></li>
									<li><label>Date du début du jeu</br>
<input id="date_login" onblur="update_config('date_login');" type="date" value="<?php if ($config["date_login"] != null) {echo date('Y-m-d', strtotime($config["date_login"]));}?>" /></label></li>
									<li><label>Date de fin du jeu</br>
<input id="date_end" onblur="update_config('date_end');" type="date" value="<?php if ($config["date_end"] != null) {echo date('Y-m-d', strtotime($config["date_end"]));}?>" /></label></li>
								</ul>
							</section>
						</div>
						<div class="4u 12u$(medium)">
							<section class="box">
								<h3>Configuration des duels</h3>
								<ul class="alt">
                                    <li><label>Délais pour fixer les termes d'un duel</br>
<input id='jours_def_duel' type='number' value='<?php echo $config["jours_def_duel"];?>' min="1" style='width:3em;' onblur="update_int('jours_def_duel');"> jour(s)</label></li>
									<li><label>Délais pour effectuer le duel</br>
<input id='jours_defi' type='number' value='<?php echo $config["jours_defi"];?>' min="1" style='width:3em;' onblur="update_int('jours_defi');"> jour(s)</label></li>
									<li><label>Délais de contestation du duel</br>
<input id='jours_contest' type='number' value='<?php echo $config["jours_contest"];?>' min="1" style='width:3em;' onblur="update_int('jours_contest');"> jour(s)</label></li>
									<li><label>Délais maximal de litige</br>
<input id='jours_litige' type='number' value='<?php echo $config["jours_litige"];?>' min="1" style='width:3em;' onblur="update_int('jours_litige');"> jour(s)</label></li>
                                    <li style="text-align:left;"><span style="text-align:center;color: #474747;display: block;font-size: 0.9em;font-weight: 700;margin: 0 0 1em 0;">Penalités</span>
                            <input type="checkbox" id="penalite" onchange="update_penalites();"/><label for="penalite">Activer pénalités contre l'attaquant en cas de litige non résolu</label>
								</ul>
							</section>
						</div>
						<div class="4u$ 12u$(medium)">
							<section class="box">
								<h3>Configuration des revendications</h3>
								<ul class="alt">
                                    <li><label>Revendiquer <input id='nb_revendication' type='number' value='<?php echo $config["nb_revendication"];?>' min="1" style='width:3em;' onblur="update_int('nb_revendication');"> territoire(s)</label></br><label>tous les
<input id='delais_revendications' type='number' value='<?php echo $config["delais_revendications"];?>' min="1" style='width:3em;' onblur="update_int('delais_revendications');"> jour(s)</label></li>
                                    <li style="text-align:left;"><span style="text-align:center;color: #474747;display: block;font-size: 0.9em;font-weight: 700;margin: 0 0 1em 0;">Jours des revendications</span>
                            <input type="checkbox" name="jours_semaine" id="Monday" value="Monday" onchange="update_jours_revendications();"/><label for="Monday">Lundi</label></br>
                            <input type="checkbox" name="jours_semaine" id="Tuesday" value="Tuesday" onchange="update_jours_revendications();"/><label for="Tuesday">Mardi</label></br>
                            <input type="checkbox" name="jours_semaine" id="Wednesday" value="Wednesday" onchange="update_jours_revendications();"/><label for="Wednesday">Mercredi</label></br>
                            <input type="checkbox" name="jours_semaine" id="Thursday" value="Thursday" onchange="update_jours_revendications();"/><label for="Thursday">Jeudi</label></br>
                            <input type="checkbox" name="jours_semaine" id="Friday" value="Friday" onchange="update_jours_revendications();"/><label for="Friday">Vendredi</label></br>
                            <input type="checkbox" name="jours_semaine" id="Saturday" value="Saturday" onchange="update_jours_revendications();"/><label for="Saturday">Samedi</label></br>
                            <input type="checkbox" name="jours_semaine" id="Sunday" value="Sunday" onchange="update_jours_revendications();"/><label for="Sunday">Dimanche</label></li>
				                </ul>
							</section>
						</div>
					</div>
				</div>
			</section>
        <!-- Six -->
            <section id="edit_map" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Editer la carte</h2>
						<p>Une édition des zones et points clés pendans une partie peut perturber son bon déroulement</p>
					</header>
				</div>
				<div class="container 50%">
                    <div class="row uniform">
                        <div class="12u$">
                            <ul class="actions">
                                <li><a href="map-editor.php"><span class="button big">Editer la carte</span></a></li>
                            </ul>
                        </div>
                    </div>
				</div>
			</section>
            <section id="reset_game" class="wrapper style6 special">
				<div class="container">
					<header class="major">
						<h2>Réinitialiser le jeu</h2>
						<p>Cette action supprimera tous les joueurs, les duels et rendra tous les territoires disponibles</p>
					</header>
				</div>
				<div class="container 50%">
                    <div class="row uniform">
                        <div class="12u$">
                            <ul class="actions">
                                <li><span class="button big" data-toggle='modal' data-target='#modal_reset_game'>Réinitialiser le jeu</span></li>
                            </ul>
                        </div>
                    </div>
				</div>
			</section>
        <?php echo $regles; ?>
		<?php echo $footer; ?>
<div class="modal fade" id="modal_reset" tabindex="-1" role="dialog" aria-labelledby="modal_reset_password" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Réinitialiser mot de passe</h5>
          </div>
          <div class="modal-body">
              Souhaitez vous réinitialiser le mot de passe de <span id="reset_name"></span>?</br>Le mot de passe de ce joueur sera vide.
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="validate_reset_password">Réinitialiser le mot de passe</button>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="modal_delete_user" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Supprimer un joueur</h5>
            </div>
            <div class="modal-body">
                Souhaitez vous supprimer le joueur <span id="delete_name"></span>?</br>Ces territoires seront libérés ou donnés à leurs attaquants respectifs.
            </div>
            <div class="modal-footer">
                <form method="post" action="#" id="form_delete_user">
                    <input name="action" value="delete_user" type="hidden">
                    <input id="id_delete_user" name="id" value="" type="hidden">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="document.getElementById('form_delete_user').submit();">Supprimer ce joueur</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_reset_game" tabindex="-1" role="dialog" aria-labelledby="modal_reset_game" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Réinitialiser le jeu</h5>
            </div>
            <div class="modal-body">
                Souhaitez vous VRAIMENT réinitialiser le jeu ?
            </div>
            <div class="modal-footer">
                <form method="post" action="#" id="form_reset_game">
                    <input name="action" value="reset_game" type="hidden">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="document.getElementById('form_reset_game').submit();">OUI, Réinitialiser le jeu</button>
                </form>
            </div>
        </div>
    </div>
</div>
	<script>
        jours_revendication= "<?php echo $config["jours_revendication"]; ?>";
        litige_penalite= "<?php echo $config["litige_penalite"]; ?>";
        if (litige_penalite == "1") {
            document.getElementById("penalite").checked = true;
        }
        $('input[name=jours_semaine]').each(function() {
            if (jours_revendication.search($(this)[0].value) != -1) {
                $(this)[0].checked = true;
            }
        });
        
		function responsive() {
			if (window.innerWidth < 1140) {
				document.getElementById("header_site_name").style.display = "none";
			}
		}
		window.onresize = responsive;
		responsive();
	</script>
	</body>
</html>