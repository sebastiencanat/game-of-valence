<?php
session_start();
include('php/config.php');
actualiser_session();

if (!isset($_GET["id"]) || !is_numeric($_GET["id"])) {
	header('Location: /espace-joueurs.php');
	exit();
}
$id_duel=intval($_GET["id"]);
$duel = sqlexec("select duels.id, duels.attaquant as attaquant_id, duels.defenseur as defenseur_id, atk.nom as attaquant_nom, IFNULL(proposedefi.nom,'') as proposedefi_nom, atk.couleur as attaquant_couleur, def.nom as defenseur_nom, def.couleur as defenseur_couleur, duels.zone_id, if(duels.zone_id,'zones','keypoints') as type_territoire, if(duels.zone_id,zones.nom,keypoints.nom) as territoire_nom, if(duels.zone_id,zones.points,keypoints.points) as territoire_points, status, IFNULL(defi,'') as defi, IFNULL(defi_type,'A définir') as defi_type, litige, duels.gagnant as gagnant_id, IFNULL(gagnant.nom,'') as gagnant_nom, DATE_FORMAT(GREATEST(creation_date,IFNULL(defi_date,0),IFNULL(fin_date,0)), '%d/%m/%Y') as update_date, DATE_FORMAT(creation_date, '%d/%m/%Y à %T') as creation_date, (creation_date > DATE_SUB(now(), INTERVAL ".$config["jours_def_duel"]." DAY)) as def_defi, DATE_FORMAT(DATE_ADD(creation_date, INTERVAL ".$config["jours_def_duel"]." DAY), '%d/%m/%Y à %T') as def_defi_limite, DATE_FORMAT(DATE_ADD(creation_date, INTERVAL ".strval($config["jours_def_duel"]*2)." DAY), '%d/%m/%Y à %T') as atk_defi_limite, DATE_FORMAT(defi_date, '%d/%m/%Y à %T') as defi_date,  DATE_FORMAT(DATE_ADD(defi_date, INTERVAL ".$config["jours_defi"]." DAY), '%d/%m/%Y à %T') as duel_limite, DATE_FORMAT(fin_date, '%d/%m/%Y à %T') as fin_date, DATE_FORMAT(DATE_ADD(fin_date, INTERVAL ".$config["jours_litige"]." DAY), '%d/%m/%Y à %T') as litige_limite, DATE_FORMAT(DATE_ADD(fin_date, INTERVAL ".$config["jours_contest"]." DAY), '%d/%m/%Y à %T') as confirm_limite from duels join joueurs as atk on duels.attaquant=atk.id join joueurs as def on duels.defenseur=def.id left join joueurs as gagnant on duels.gagnant=gagnant.id left join joueurs as proposedefi on duels.defi_par = proposedefi.id left join zones on duels.zone_id=zones.id left join keypoints on duels.keypoint_id=keypoints.id where duels.id=$id_duel;")[0];


// inserer ici le traitement retour formulaire
if (isset($_POST["action"]) && $_POST["action"]=="definir_duel" && isset($_POST["defi_type"]) && isset($_POST["defi"])) {
	if ($_SESSION["id"] == $duel["defenseur_id"] && $duel["def_defi"] == "1") { //si joueur défenseur et qu'il peut définir le duel
		sqlexec("update duels set status='DEFI', defi_par=".$_SESSION["id"].", defi_type=".$conn->quote($_POST["defi_type"]).", defi=".$conn->quote($_POST["defi"]).", defi_date=now() where id=".$id_duel);
	} else if ($_SESSION["id"] == $duel["attaquant_id"] && $duel["def_defi"] == "0") { //si joueur attaquant et qu'il peut définir le duel
		sqlexec("update duels set status='DEFI', defi_par=".$_SESSION["id"].", defi_type=".$conn->quote($_POST["defi_type"]).", defi=".$conn->quote($_POST["defi"]).", defi_date=now() where id=".$id_duel);
	}
	header('Location: duel.php?id='.$id_duel);
	exit();
}

if (isset($_POST["action"]) && $_POST["action"]=="designer_vainqueur" && isset($_POST["joueur"]) && is_numeric($_POST["joueur"])) {
	if ($_SESSION["id"] == $duel["defenseur_id"]) { //si joueur défenseur
		if ($duel["status"] == "DEFI") { // 1ere proposition de vainqueur
			if ($_POST["joueur"] == $_SESSION["id"]) {
				sqlexec("update duels set status='DEFENSEUR_AVIS', gagnant=".$conn->quote($_POST["joueur"]).", fin_date=now() where id=".$id_duel);
			} else {
				sqlexec("update duels set status='END', gagnant=".$conn->quote($_POST["joueur"]).", fin_date=now() where id=".$id_duel);
			}
		} else if ($_POST["joueur"] == $duel["gagnant_id"] && $duel["status"] == "ATTAQUANT_AVIS") {
			sqlexec("update duels set status='END', litige=0 where id=".$id_duel);
		} else if ($_POST["joueur"] != $duel["gagnant_id"] && $duel["status"] == "ATTAQUANT_AVIS") {
			sqlexec("update duels set status='DEFENSEUR_AVIS', gagnant=".$conn->quote($_POST["joueur"]).", litige=1 where id=".$id_duel);
		}
	} else if ($_SESSION["id"] == $duel["attaquant_id"]) { //si joueur attaquant
		if ($duel["status"] == "DEFI") { // 1ere proposition de vainqueur
			if ($_POST["joueur"] == $_SESSION["id"]) {
				sqlexec("update duels set status='ATTAQUANT_AVIS', gagnant=".$conn->quote($_POST["joueur"]).", fin_date=now() where id=".$id_duel);
			} else {
				sqlexec("update duels set status='END', gagnant=".$conn->quote($_POST["joueur"]).", fin_date=now() where id=".$id_duel);
			}
		} else if ($_POST["joueur"] == $duel["gagnant_id"] && $duel["status"] == "DEFENSEUR_AVIS") {
			sqlexec("update duels set status='END', litige=0 where id=".$id_duel);
		} else if ($_POST["joueur"] != $duel["gagnant_id"] && $duel["status"] == "DEFENSEUR_AVIS") {
			sqlexec("update duels set status='ATTAQUANT_AVIS', gagnant=".$conn->quote($_POST["joueur"]).", litige=1 where id=".$id_duel);
		}
	}
	header('Location: /espace-joueurs.php');
	exit();
}
if (isset($_POST["action"]) && $_POST["action"]=="moderation" && isset($_POST["joueur"]) && is_numeric($_POST["joueur"])) {
	if ($_SESSION["id"] == "0") {
		sqlexec("update duels set status='END', gagnant=".$conn->quote($_POST["joueur"]).", fin_date=now() where id=".$id_duel);
        if ($config["litige_penalite"]=="1" && $_POST["joueur"] == "0") {
            sqlexec("select @attaquant_id := duels.attaquant, @points := if(duels.zone_id,zones.points,keypoints.points) as territoire_points from duels join joueurs as atk on duels.attaquant=atk.id join joueurs as def on duels.defenseur=def.id left join joueurs as gagnant on duels.gagnant=gagnant.id left join joueurs as proposedefi on duels.defi_par = proposedefi.id left join zones on duels.zone_id=zones.id left join keypoints on duels.keypoint_id=keypoints.id where duels.id=$id_duel; update joueurs set point_penalite=point_penalite+@points where id=@attaquant_id;");
        }
	}
	header('Location: admin.php');
	exit();
}

$liste_info_zone="";
if ($duel["type_territoire"] == "zones"){
	$type_territoire_header = "la zone";
	$type_territoire_resume = "Zone";
	$info_zone = sqlexec("select (select count(*) from zones join frontieres on zones.id=frontieres.id1 where zones.id=".$duel["zone_id"].") as nb_zones_voisines, (select count(*) from keypoints where zone_id=".$duel["zone_id"].") as nb_pt_cle, GROUP_CONCAT(nom SEPARATOR ', ') as voisins from ( select joueurs.nom from zones join frontieres on zones.id=frontieres.id1 join zones as voisin on voisin.id=frontieres.id2 join joueurs on voisin.proprietaire=joueurs.id where joueurs.id != 0 and zones.id=".$duel["zone_id"]." union select joueurs.nom from keypoints join joueurs on keypoints.proprietaire = joueurs.id where joueurs.id != 0 and zone_id=".$duel["zone_id"].") as voisins;")[0];
	$liste_info_zone="<li>Nombre de zones voisines: ".$info_zone["nb_zones_voisines"]."</li><li>Nombre de point clé dans la zone: ".$info_zone["nb_pt_cle"]."</li><li>Joueurs pouvant attaquer la zone: ".$info_zone["voisins"]."</li>";
} else {
	$type_territoire_header = "le point clé";
	$type_territoire_resume = "Point clé";
}
$action_duel="";
if ($_SESSION["id"] == "0") {
    $msg_penalite="";
    if ($config["litige_penalite"] == "1") {
        $msg_penalite=" et $duel[attaquant_nom] subira $duel[territoire_points] point(s) de pénalité";
    }
    $action_duel = <<<ACTIONDUEL
<section id="action_admin" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Modération par le MJ</h2>
		</header>
	</div>
	<div class="container">
		<h3>En tant que MJ, vous pouvez modérer le duel qui oppose $duel[attaquant_nom] à $duel[defenseur_nom] pour $type_territoire_header $duel[territoire_nom]<h3>
		<form method="post" action="#">
		<input name="action" value="moderation" type="hidden">
			<div class="row uniform 50%">
				<div class="4u">
					<input type="radio" id="attaquant_checkbox" name="joueur" value="$duel[attaquant_id]">
					<label for="attaquant_checkbox">$duel[attaquant_nom] gagne le territoire</label>
				</div>
                <div class="4u">
					<input type="radio" id="defenseur_checkbox" name="joueur" value="$duel[defenseur_id]">
					<label for="defenseur_checkbox">$duel[defenseur_nom] conserve son territoire</label>
				</div>
				<div class="4u$">
					<input type="radio" id="nul_checkbox" name="joueur" value="0">
					<label for="nul_checkbox">Le territoire redeviens libre$msg_penalite</label>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
}


if ($_SESSION["id"] == $duel["defenseur_id"]) { // Si joueur est défenseur
	switch ($duel["status"]){
		case "NEW":
			if ($duel["def_defi"] == "1"){
				$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		<h3>Vous devez déninir les termes du duel avant le $duel[def_defi_limite] ou ça sera $duel[attaquant_nom] qui le fera</h3>
		<form method="post" action="#">
		<input name="action" value="definir_duel" type="hidden">
			<div class="row uniform 50%">
				<div class="12u$">
					<div class="6u" style="margin:auto;">
						<input type="text" name="defi_type" value="" placeholder="Type de duel (Sec, Hand, MarioKart, Fifa, ...)">
					</div>
				</div>
				<div class="12u$">
					<textarea name="defi" id="defi" placeholder="Les détails du duel (Date et heure, pensez que le duel doit avoir lieu dans les $config[jours_defi] jours maximum, règles additionnelles etc.)" rows="6"></textarea>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			}
			break;
		case "DEFI":
			$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		<h3>Vous devez déninir le vainqueur du duel avant le $duel[duel_limite] ou ça sera $duel[proposedefi_nom] qui sera désigné vainqueur</h3>
		<form method="post" action="#">
		<input name="action" value="designer_vainqueur" type="hidden">
			<div class="row uniform 50%">
				<div class="6u">
					<input type="radio" id="attaquant_checkbox" name="joueur" value="$duel[attaquant_id]">
					<label for="attaquant_checkbox">$duel[attaquant_nom] est le vainqueur</label>
				</div>
				<div class="6u$">
					<input type="radio" id="defenseur_checkbox" name="joueur" value="$duel[defenseur_id]">
					<label for="defenseur_checkbox">$duel[defenseur_nom] est le vainqueur</label>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			break;
		case "DEFENSEUR_AVIS":
            $msg_penalite="";
            if ($config["litige_penalite"] == "1") {
                $msg_penalite=" et $duel[attaquant_nom] subira $duel[territoire_points] point(s) de pénalité";
            }
			if ($duel["litige"] == "0") {
				$titre_formulaire = "<h3>Vous avez indiqué être le vainqueur du duel. Néanmoins, $duel[attaquant_nom] peut contester votre victoire jusqu'au $duel[confirm_limite] </h3><br><h3>Attention, en cas de litige, vous aurez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			} else {
				$titre_formulaire = "<h3>Vous avez indiqué être le vainqueur du duel. Néanmoins, $duel[attaquant_nom] conteste votre victoire.</h3><br><h3>Vous avez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			}
			$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		$titre_formulaire
		<form method="post" action="#">
		<input name="action" value="designer_vainqueur" type="hidden">
			<div class="row uniform 50%">
				<div class="6u">
					<input type="radio" id="attaquant_checkbox" name="joueur" value="$duel[attaquant_id]">
					<label for="attaquant_checkbox">$duel[attaquant_nom] est le vainqueur</label>
				</div>
				<div class="6u$">
					<input type="radio" id="defenseur_checkbox" name="joueur" value="$duel[defenseur_id]">
					<label for="defenseur_checkbox">$duel[defenseur_nom] est le vainqueur</label>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			break;
		case "ATTAQUANT_AVIS":
            $msg_penalite="";
            if ($config["litige_penalite"] == "1") {
                $msg_penalite=" et $duel[attaquant_nom] subira $duel[territoire_points] point(s) de pénalité";
            }
			if ($duel["litige"] == "0") {
				$titre_formulaire = "<h3>$duel[attaquant_nom] a indiqué être le vainqueur du duel, vous pouvez confirmer ou infirmer sa victoire.</h3><br><h3>Attention, si vous indiquer avoir remporté le duel, vous aurez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			} else {
				$titre_formulaire = "<h3>$duel[attaquant_nom] conteste votre victoire et a indiqué être le vainqueur du duel, vous pouvez confirmer ou infirmer sa victoire.</h3><br><h3>Attention, si vous indiquer avoir remporté le duel, vous aurez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			}
			$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		$titre_formulaire
		<form method="post" action="#">
		<input name="action" value="designer_vainqueur" type="hidden">
			<div class="row uniform 50%">
				<div class="6u">
					<input type="radio" id="attaquant_checkbox" name="joueur" value="$duel[attaquant_id]">
					<label for="attaquant_checkbox">$duel[attaquant_nom] est le vainqueur</label>
				</div>
				<div class="6u$">
					<input type="radio" id="defenseur_checkbox" name="joueur" value="$duel[defenseur_id]">
					<label for="defenseur_checkbox">$duel[defenseur_nom] est le vainqueur</label>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			break;
		default:
			break;
	}
}

if ($_SESSION["id"] == $duel["attaquant_id"]) { // Si joueur est attaquant
	switch ($duel["status"]){
		case "NEW":
			if ($duel["def_defi"] == "0"){
			$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		<h3>$duel[defenseur_nom] n'a pas défini les termes du duel à temps, c'est donc à vous de le faire. Vous avez jusqu'au $duel[atk_defi_limite] sinon le suel sera annulé !</h3>
		<form method="post" action="#">
		<input name="action" value="definir_duel" type="hidden">
			<div class="row uniform 50%">
				<div class="12u$">
					<div class="6u" style="margin:auto;">
						<input type="text" name="defi_type" value="" placeholder="Type de duel (Sec, Hand, MarioKart, Fifa, ...)">
					</div>
				</div>
				<div class="12u$">
					<textarea name="defi" id="defi" placeholder="Les détails du duel (Date et heure, pensez que le duel doit avoir lieu dans les $config[jours_defi] jours maximum, règles additionnelles etc.)" rows="6"></textarea>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			}
			break;
		case "DEFI":
			$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		<h3>Vous devez déninir le vainqueur du duel avant le $duel[duel_limite] ou ça sera $duel[proposedefi_nom] qui sera désigné vainqueur</h3>
		<form method="post" action="#">
		<input name="action" value="designer_vainqueur" type="hidden">
			<div class="row uniform 50%">
				<div class="6u">
					<input type="radio" id="attaquant_checkbox" name="joueur" value="$duel[attaquant_id]">
					<label for="attaquant_checkbox">$duel[attaquant_nom] est le vainqueur</label>
				</div>
				<div class="6u$">
					<input type="radio" id="defenseur_checkbox" name="joueur" value="$duel[defenseur_id]">
					<label for="defenseur_checkbox">$duel[defenseur_nom] est le vainqueur</label>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			break;
		case "ATTAQUANT_AVIS":
            $msg_penalite="";
            if ($config["litige_penalite"] == "1") {
                $msg_penalite=" et vous subirez $duel[territoire_points] point(s) de pénalité";
            }
			if ($duel["litige"] == "0") {
				$titre_formulaire = "<h3>Vous avez indiqué être le vainqueur du duel. Néanmoins, $duel[defenseur_nom] peut contester votre victoire jusqu'au $duel[confirm_limite] </h3><br><h3>Attention, en cas de litige, vous aurez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			} else {
				$titre_formulaire = "<h3>Vous avez indiqué être le vainqueur du duel. Néanmoins, $duel[defenseur_nom] conteste votre victoire.</h3><br><h3>Vous avez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			}
			$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		$titre_formulaire
		<form method="post" action="#">
		<input name="action" value="designer_vainqueur" type="hidden">
			<div class="row uniform 50%">
				<div class="6u">
					<input type="radio" id="attaquant_checkbox" name="joueur" value="$duel[attaquant_id]">
					<label for="attaquant_checkbox">$duel[attaquant_nom] est le vainqueur</label>
				</div>
				<div class="6u$">
					<input type="radio" id="defenseur_checkbox" name="joueur" value="$duel[defenseur_id]">
					<label for="defenseur_checkbox">$duel[defenseur_nom] est le vainqueur</label>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			break;
		case "DEFENSEUR_AVIS":
            $msg_penalite="";
            if ($config["litige_penalite"] == "1") {
                $msg_penalite=" et vous subirez $duel[territoire_points] point(s) de pénalité";
            }
			if ($duel["litige"] == "0") {
				$titre_formulaire = "<h3>$duel[defenseur_nom] a indiqué être le vainqueur du duel, vous pouvez confirmer ou infirmer sa victoire.</h3><br><h3>Attention, si vous indiquer avoir remporté le duel, vous aurez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			} else {
				$titre_formulaire = "<h3>$duel[defenseur_nom] conteste votre victoire et a indiqué être le vainqueur du duel, vous pouvez confirmer ou infirmer sa victoire.</h3><br><h3>Attention, si vous indiquer avoir remporté le duel, vous aurez jusqu'au $duel[litige_limite] pour vous mettre d'accord ou bien $type_territoire_header $duel[territoire_nom] sera libéré$msg_penalite<h3>";
			}
			$action_duel = <<<ACTIONDUEL
<section id="action_necessaire" class="wrapper style2 special">
	<div class="container">
		<header class="major">
			<h2>Action nécessaire</h2>
		</header>
	</div>
	<div class="container">
		$titre_formulaire
		<form method="post" action="#">
		<input name="action" value="designer_vainqueur" type="hidden">
			<div class="row uniform 50%">
				<div class="6u">
					<input type="radio" id="attaquant_checkbox" name="joueur" value="$duel[attaquant_id]">
					<label for="attaquant_checkbox">$duel[attaquant_nom] est le vainqueur</label>
				</div>
				<div class="6u$">
					<input type="radio" id="defenseur_checkbox" name="joueur" value="$duel[defenseur_id]">
					<label for="defenseur_checkbox">$duel[defenseur_nom] est le vainqueur</label>
				</div>
				<div class="12u$">
					<ul class="actions">
						<li><input type="submit" value="Envoyer" class="special" /></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>
ACTIONDUEL;
			break;
		default:
			break;
	}
}
?>
<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Espace joueurs - Game of <?php echo $nom_ville; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<script>
function getResumeJoueurs(){
	$.ajax({
		type: "POST",
		url: "php/getClassement.php",
		success: result,
		dataType: "json"
	});

	function result(data){
		var Classement_attaquant;
		var Territoires_attaquant;
		var Classement_defenseur;
		var Territoires_defenseur;
		var position;
		nbjoueurs=data.length;
		position = 0;
		data.forEach(function (joueur){
			position++;
			if (joueur.id == "<?php echo $duel["attaquant_id"]; ?>") {
				Classement_attaquant = "Classement: "+position+"/"+nbjoueurs+" avec un score de "+joueur.Score+"&nbsp; points.";
				Territoires_attaquant = joueur.Territoires;
			}
			if (joueur.id == "<?php echo $duel["defenseur_id"]; ?>") {
				Classement_defenseur = "Classement: "+position+"/"+nbjoueurs+" avec un score de "+joueur.Score+"&nbsp; points.";
				Territoires_defenseur = joueur.Territoires;
			}
		})
		document.getElementById("classement-attaquant").innerHTML = Classement_attaquant;
		document.getElementById("territoires-attaquant").innerHTML = "Territoires conquis: "+Territoires_attaquant;
		document.getElementById("classement-defenseur").innerHTML = Classement_defenseur;
		document.getElementById("territoires-defenseur").innerHTML = "Territoires conquis: "+Territoires_defenseur;
	}
}
getResumeJoueurs();
var refresh_ResumeJoueurs = setInterval(getResumeJoueurs, 60000);
		</script>
				
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header">
			<h1 id="header_site_name"><a href="espace-joueurs.php">Game of <?php echo $nom_ville; ?></a></h1>
				<nav id="nav">
					<ul>
						<li><a href="espace-joueurs.php#revendications">Revendiquer un territoire</a></li>
						<li><a href="espace-joueurs.php#mes_duels">Mes Duels</a></li>
						<li><a href="espace-joueurs.php#le_classement">Le Classement</a></li>
						<li><a href="espace-joueurs.php#les_regles">Les Règles</a></li>
						<li><a href="mon-compte.php">Mon Compte</a></li>
						<li><a href="logout.php" class="button special">Déconnexion</a></li>
					</ul>
				</nav>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h2>Duel opposant</h2>
				<h2><?php echo $duel["attaquant_nom"]." à ".$duel["defenseur_nom"]; ?></h2>
				<p><?php echo "Pour ".$type_territoire_header." ".$duel["territoire_nom"]." - ".$duel["territoire_points"]." point(s)"; ?></p>
			</section>
		<!-- Résumé Duel -->	
			<section id="one" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Résumé du duel</h2>
					</header>
					<div class="row 150%">
						<div class="3u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded fa-duel" style="background: linear-gradient(to right, <?php echo $duel["attaquant_couleur"]; ?> 50%, <?php echo $duel["defenseur_couleur"]; ?> 50%);"></i>
								<h3>Duel</h3>
								<ul class="alt">
									<li>Lancé le <?php echo $duel["creation_date"];?></li>
									<li>Défini par <?php echo $duel["proposedefi_nom"];?></li>
									<li>Défini le <?php echo $duel["defi_date"];?></li>
									<li>Type de duel: <?php echo $duel["defi_type"];?></li>
									<li>Duel fini le: <?php echo $duel["fin_date"];?></li>
									<li>Vainqueur: <?php if ($duel["gagnant_id"] == "0") { echo "Personne, Territoire libéré"; } else { echo $duel["gagnant_nom"]; }?></li>
								</ul>
							</section>
						</div>
						<div class="3u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded fa-sword" style="background-color: <?php echo $duel["attaquant_couleur"]; ?>;"></i>
								<h3>Attaquant</h3>
								<ul class="alt">
									<li>Nom: <?php echo $duel["attaquant_nom"];?></li>
									<li id="classement-attaquant"></li>
									<li id="territoires-attaquant"></li>
								</ul>
							</section>
						</div>
						<div class="3u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded fa-shield" style="background-color: <?php echo $duel["defenseur_couleur"]; ?>;"></i>
								<h3>Défenseur</h3>
								<ul class="alt">
									<li>Nom: <?php echo $duel["defenseur_nom"];?></li>
									<li id="classement-defenseur"></li>
									<li id="territoires-defenseur"></li>
								</ul>
							</section>
						</div>
						<div class="3u$ 12u$(medium)">
							<section class="box">
								<i class="icon big rounded fa-home" style="background-color: black;"></i>
								<h3>Territoire en jeu</h3>
								<ul class="alt">
									<li>Nom: <?php echo $duel["territoire_nom"];?></li>
									<li>Type de territoire: <?php echo $type_territoire_resume?></li>
									<li>Nombre de point(s): <?php echo $duel["territoire_points"]?></li>
									<?php echo $liste_info_zone;?>
								</ul>
							</section>
						</div>
						<div class="12u$">
							<h3>Détails du duel</h3>
							<textarea readonly placeholder="Les détails du duel n'ont pas encore été définis" rows="6"><?php echo $duel["defi"]; ?></textarea>
						</div>
					</div>
				</div>
			</section>
			<?php echo $action_duel; ?>
		<?php echo $footer; ?>
	<script>
		function responsive() {
			if (window.innerWidth < 1140) {
				document.getElementById("header_site_name").style.display = "none";
			}
		}
		window.onresize = responsive;
		responsive();
	</script>
	</body>
</html>