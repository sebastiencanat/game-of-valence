<?php
include_once('config.php');
$sql = 'select id, couleur, IFNULL((SUM(points)-point_penalite), 0) as Score, point_penalite, Nom, IFNULL(GROUP_CONCAT(territoires SEPARATOR ", "), "Aucun") as Territoires from (
select joueurs.couleur as couleur, joueurs.id as id, joueurs.nom as nom, SUM(keypoints.points) as points, GROUP_CONCAT(keypoints.nom SEPARATOR ", ") as territoires, point_penalite as point_penalite from joueurs left join keypoints on keypoints.proprietaire=joueurs.id where joueurs.id!=0 group by id
union all
select joueurs.couleur as couleur, joueurs.id as id, joueurs.nom as nom, SUM(zones.points) as points, GROUP_CONCAT(zones.nom SEPARATOR ", ") as territoires, point_penalite as point_penalite from joueurs left join zones on zones.proprietaire=joueurs.id where joueurs.id!=0 group by id
) as Classement group by id order by Score desc, nom asc';
echo json_encode(sqlexec($sql));
?>