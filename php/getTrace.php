<?php
// fichier de config où se trouve le mot de passe et les paramètres de connexion à la bdd
include_once('config.php');

$sql = 'select zones.id as id,zones.Nom as Nom, geo,points,joueurs.nom as proprietaire, joueurs.id as proprietaire_id, joueurs.couleur as couleur, IFNULL(attk.nom, "") as attaquant from zones join joueurs on zones.proprietaire=joueurs.id left join duels on zones.duel_id=duels.id left join joueurs as attk on duels.attaquant=attk.id order by id;';
$req = $conn->prepare($sql);
$req->execute();


//Concstuction du GeoJSON
$geojson = array(
    'type'      => 'FeatureCollection',
    'features'  => array()
);

// boucle pour traiter le résultat de la requete et remplir le geojson
while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
    $properties = $row;
    //On enlève les propriétés des géométries
    unset($properties['geo']);
    $feature = array(
        'type' => 'Feature',
        'geometry' => json_decode('{"type":"Polygon","coordinates":'.$row['geo'].'}'),
        'properties' => $properties
    );
    // on push la feature
    array_push($geojson['features'], $feature);
}

echo json_encode($geojson);
?>