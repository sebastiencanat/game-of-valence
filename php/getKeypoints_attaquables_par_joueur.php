<?php
session_start();
include_once('config.php');
if (isset($_SESSION["id"]) && is_numeric($_SESSION["id"])) {
    $sql = "select keypoints.id, keypoints.nom, keypoints.points, keypoints.proprietaire, joueurs.nom as nom_proprietaire from keypoints left join joueurs on keypoints.proprietaire=joueurs.id where keypoints.proprietaire!=$_SESSION[id] and keypoints.duel_id is null order by points desc, nom asc";
    echo json_encode(sqlexec($sql));
}
?>