<?php
// fichier de config où se trouve le mot de passe et les paramètres de connexion à la bdd
include_once('config.php');

$sql = 'select keypoints.id as id,keypoints.Nom as Nom, geo,points,joueurs.nom as proprietaire, joueurs.id as proprietaire_id, joueurs.couleur as couleur, IFNULL(attk.nom, "") as attaquant from keypoints join joueurs on keypoints.proprietaire=joueurs.id left join duels on keypoints.duel_id=duels.id left join joueurs as attk on duels.attaquant=attk.id order by id;';
$req = $conn->prepare($sql);
$req->execute();


//Concstuction du GeoJSON
$json = array(
    'type'      => 'FeatureCollection',
    'features'  => array()
);

// boucle pour traiter le résultat de la requete et remplir le geojson
while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
    $properties = $row;
	$feature = array(
        'type' => 'Feature',
        'geometry' => json_decode('{"type":"Point","coordinates":'.$row['geo'].'}'),
        'properties' => $properties
    );
    array_push($json['features'], $feature);
}

echo json_encode($json);
?>