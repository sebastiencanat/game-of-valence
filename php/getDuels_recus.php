<?php
session_start();
include_once('config.php');
if (isset($_SESSION["id"]) && is_numeric($_SESSION["id"])) {
    $sql = "select creation_date > DATE_SUB(now(), INTERVAL 2 DAY) as defi_def, duels.id, duels.attaquant as attaquant_id, joueurs.nom as attaquant_nom, if(duels.zone_id,'Zone','Point clé') as type_territoire, if(duels.zone_id,zones.nom,keypoints.nom) as territoire_nom, if(duels.zone_id,zones.points,keypoints.points) as territoire_points, status, IFNULL(defi_type,'A définir') as defi_type, litige, gagnant, DATE_FORMAT(GREATEST(creation_date,IFNULL(defi_date,0),IFNULL(fin_date,0)), '%d/%m/%Y') as update_date, creation_date, defi_date, fin_date from duels join joueurs on duels.attaquant=joueurs.id left join zones on duels.zone_id=zones.id left join keypoints on duels.keypoint_id=keypoints.id where duels.status!='END' and duels.defenseur=$_SESSION[id] order by update_date desc;";
    echo json_encode(sqlexec($sql));
}
?>