<?php
include_once('config.php');
$sql = "select couleur, points, nom, if(proprietaire_id=0,'',proprietaire) as proprietaire from (
select keypoints.points as points, keypoints.nom as nom, joueurs.nom as proprietaire, joueurs.couleur as couleur, keypoints.proprietaire as proprietaire_id from keypoints left join joueurs on keypoints.proprietaire = joueurs.id where duel_id is null
union all
select zones.points as points, zones.nom as nom, joueurs.nom as proprietaire, joueurs.couleur as couleur, zones.proprietaire as proprietaire_id from zones left join joueurs on zones.proprietaire = joueurs.id where duel_id is null
) as territoires_attaquables order by points desc, nom asc";
echo json_encode(sqlexec($sql));
?>