<?php
session_start();
include_once('config.php');
if (isset($_SESSION["id"]) && is_numeric($_SESSION["id"])) {
    $sql = "select * from (
    select zones.id, zones.nom, zones.points, zones.proprietaire, joueurs.nom as nom_proprietaire from keypoints left join zones on keypoints.zone_id = zones.id left join joueurs on zones.proprietaire = joueurs.id where zones.duel_id is null and zones.proprietaire!=$_SESSION[id] and keypoints.proprietaire=$_SESSION[id]
    union
    select zones.id, zones.nom, zones.points, zones.proprietaire, joueurs.nom as nom_proprietaire from frontieres left join zones on frontieres.id2=zones.id left join joueurs on zones.proprietaire = joueurs.id where zones.duel_id is null and zones.proprietaire!=$_SESSION[id] and frontieres.id1 in (select id from zones where proprietaire=$_SESSION[id])
    union
    select zones.id, zones.nom, zones.points, zones.proprietaire, joueurs.nom as nom_proprietaire from frontieres left join zones on frontieres.id1=zones.id left join joueurs on zones.proprietaire = joueurs.id where zones.duel_id is null and zones.proprietaire!=$_SESSION[id] and frontieres.id2 in (select id from zones where proprietaire=$_SESSION[id])
    ) as zones_attaquables order by points desc, nom asc";
    echo json_encode(sqlexec($sql));
}
?>