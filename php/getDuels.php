<?php
session_start();
include_once('config.php');
if (isset($_SESSION["id"]) && is_numeric($_SESSION["id"])) {
    $sql = "select duels.id, duels.attaquant as attaquant_id, duels.defenseur as defenseur_id, atk.nom as attaquant_nom, def.nom as defenseur_nom, if(duels.zone_id,'Zone','Point clé') as type_territoire, if(duels.zone_id,zones.nom,keypoints.nom) as territoire_nom, if(duels.zone_id,zones.points,keypoints.points) as territoire_points, status, IFNULL(defi,'A définir') as defi, litige, gagnant, DATE_FORMAT(GREATEST(creation_date,IFNULL(defi_date,0),IFNULL(fin_date,0)), '%d/%m/%Y') as update_date, creation_date, defi_date, fin_date from duels join joueurs as atk on duels.attaquant=atk.id join joueurs as def on duels.defenseur=def.id left join zones on duels.zone_id=zones.id left join keypoints on duels.keypoint_id=keypoints.id where duels.status!='END' order by update_date desc;";
    echo json_encode(sqlexec($sql));
}
?>