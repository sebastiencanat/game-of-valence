<?php
session_start();
include_once('config.php');
if (isset($_SESSION["id"]) && is_numeric($_SESSION["id"])) {
    $sql = "select (
    (
        not (
            (
                select IFNULL(now() > date_eliminatoires,1) from config
            ) and not (
                select count(*) > 0 from (
                    select id from zones where proprietaire=$_SESSION[id] union all select id from keypoints where proprietaire=$_SESSION[id]
                ) as territoires
            )
        )
    ) and (
        (
            FIND_IN_SET(DAYNAME(now()),(select jours_revendication from config))
        ) and (
                (
                ifnull(date(last_revendication), 1) < DATE_SUB(now(), INTERVAL $config[delais_revendications] DAY)
            ) or (
                num_revendication < $config[nb_revendication]
            )
        )
    )
) as peut_revendiquer from joueurs where id=$_SESSION[id];";
    echo json_encode(sqlexec($sql)[0]);
}
?>