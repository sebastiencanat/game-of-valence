<?php
$cfg_db_host = '';
$cfg_db_port = '';
$cfg_db_db = '';
$cfg_db_user = '';
$cfg_db_password = '';

$nom_ville="Valence";
try{
    $conn = new PDO('mysql:host='.$cfg_db_host.';port='.$cfg_db_port.';charset=utf8;dbname='.$cfg_db_db, $cfg_db_user , $cfg_db_password);
}
catch (Exception   $e){echo json_encode(array("retour"=>'Erreur de connexion à la bdd', "e" => $e )); die();}

function reset_game() {
    sqlexec("delete from joueurs where id > 0; delete from duels; ALTER TABLE joueurs AUTO_INCREMENT=1;ALTER TABLE duels AUTO_INCREMENT=1; update config set date_reset=null;");
}

// Récupération config
$config=sqlexec("select (date_reset < now()) as need_reset, (IFNULL(now() > date_inscription,1) and IFNULL(now() < date_fin_inscription,1) and IFNULL(now() < date_end,1)) as peut_inscrire, (IFNULL(now() > date_login,1) and IFNULL(now() < date_end,1)) as peut_connecter, IFNULL(now() > date_eliminatoires, 0) as phase_eliminatoire, IFNULL(DATE_FORMAT(date_eliminatoires, '%d/%m/%Y à %T'),0) as print_date_eliminatoires, IFNULL(DATE_FORMAT(date_inscription, '%d/%m/%Y à %T'),0) as print_date_inscription, IFNULL(DATE_FORMAT(date_fin_inscription, '%d/%m/%Y à %T'),0) as print_date_fin_inscription, IFNULL(DATE_FORMAT(date_login, '%d/%m/%Y à %T'),0) as print_date_login, IFNULL(DATE_FORMAT(date_end, '%d/%m/%Y à %T'),0) as print_date_end, config.* from config")[0];
if ($config["need_reset"] == "1") {
    reset_game();
}

function sqlexec($sql){
	global $conn;
	$req = $conn->prepare($sql);
	$req->execute();
	$retour = $req->fetchAll(PDO::FETCH_ASSOC);
	return $retour;
}
function actualiser_session() {
    global $config;
	if(isset($_SESSION['id'])) //Vérification id
	{
		//utilisation de la fonction sqlquery, on sait qu'on aura qu'un résultat car l'id d'un membre est unique.
		$retour = sqlexec("select id, nom, password, couleur, last_login from joueurs WHERE id = ".intval($_SESSION['id']), 1)[0];
		
		//Si la requête a un résultat (c'est-à-dire si l'id existe dans la table membres)
		if(isset($retour['nom']) && $retour['nom'] != '')
		{
			if($_SESSION['password'] != $retour['password'])
			{
				//Mot de passe de session incorrect
				
				vider_cookie();
				session_destroy();
				header('Location: /login.php');
				exit();
			}
			
			else
			{
                if ($config["peut_connecter"] == "1" || $retour["id"] == "0") {
                    //Validation de la session.
                        $_SESSION['id'] = $retour['id'];
                        $_SESSION['nom'] = $retour['nom'];
                        $_SESSION['password'] = $retour['password'];
                        $_SESSION['couleur'] = $retour['couleur'];
                        $_SESSION['last_login'] = $retour['last_login'];
                        sqlexec("update joueurs set last_login=now() where id=".$retour['id'].";");
                } else { // Connexion interdite par admin (fin du jeu)
                    vider_cookie();
				    session_destroy();
                    header('Location: /index.php');
                    exit();
                }
			}
		} else {
			vider_cookie();
			session_destroy();
			header('Location: /login.php');
			exit();
		}
	}
	
	else //On vérifie les cookies et sinon pas de session
	{
		if(isset($_COOKIE['id']) && isset($_COOKIE['password'])) //S'il en manque un, pas de session.
		{
			//idem qu'avec les $_SESSION
			$retour = sqlexec("select id, nom, password, couleur, last_login from joueurs WHERE id = ".intval($_COOKIE['id']), 1)[0];
			
			if(isset($retour['nom']) && $retour['nom'] != '')
			{
				if($_COOKIE['password'] != $retour['password'])
				{
					//Dehors vilain tout moche !
					vider_cookie();
					session_destroy();
					header('Location: /login.php');
					exit();
				}
				
				else
				{
					if ($config["peut_connecter"] == "1" || $retour["id"] == "0") {
                    //Validation de la session.
                        $_SESSION['id'] = $retour['id'];
                        $_SESSION['nom'] = $retour['nom'];
                        $_SESSION['password'] = $retour['password'];
                        $_SESSION['couleur'] = $retour['couleur'];
                        $_SESSION['last_login'] = $retour['last_login'];
                        sqlexec("update joueurs set last_login=now() where id=".$retour['id'].";");
                    } else { // Connexion interdite par admin (fin du jeu)
                        vider_cookie();
				        session_destroy();
                        header('Location: /index.php');
                        exit();
                    }
				}
			} else {
				vider_cookie();
				session_destroy();
				header('Location: /login.php');
				exit();
			}
		}
		
		else
		{
			//Fonction de suppression de toutes les variables de cookie.
			if(isset($_SESSION['id'])) unset($_SESSION['id']);
			vider_cookie();
			session_destroy();
			header('Location: /login.php');
			exit();
		}
	}
}

function vider_cookie()
{
	foreach($_COOKIE as $cle => $element)
	{
		setcookie($cle, '', time()-3600);
	}
}

    
// si le jeu est fini on retire les territoires litigieux
if ($config["peut_connecter"] == "0") {
    sqlexec("update duels set gagnant=0, litige=0 where litige=1;");
}

// En cas de suppression d'un joueur, ses revendications sont supprimés et ses terres revendiquée sont transférés vers leurs attaquant respectifs ou rendu disponible
// En cas de duel contre le joueur 0, duel supprimé et attaquant devient proprietaire

sqlexec("update zones left join duels on zones.duel_id=duels.id set zones.proprietaire=IFNULL(duels.attaquant, 0), zones.duel_id = null where zones.proprietaire is null or zones.proprietaire=0");
sqlexec("update keypoints left join duels on keypoints.duel_id=duels.id set keypoints.proprietaire=IFNULL(duels.attaquant, 0), keypoints.duel_id = null where keypoints.proprietaire is null or keypoints.proprietaire=0");
sqlexec("delete from duels where attaquant is null or defenseur is null or attaquant=0 or defenseur=0");

 // Si un duel n'est pas défini dans le temps impartis, l'attaque est annulée
sqlexec("delete from duels where duels.status='NEW' and creation_date < DATE_SUB(now(), INTERVAL ".strval($config["jours_def_duel"]*2)." DAY)");

 // Si vainqueur n'est pas nommé dans le temps imparti après la définition du defi, je noueur qui a défini le défi le gagne
sqlexec("update duels set gagnant=defi_par, status='END' where duels.status='DEFI' and defi_date < DATE_SUB(now(), INTERVAL ".$config["jours_defi"]." DAY);");

// si la victoire d'un duel n'a pas été confirmé par le 2em joueur dans le temps imparti suivant la déposition de l'avis du 1er joueur, le duel est terminé et le gagnant est déterminé par l'avis du premier joueur
sqlexec("update duels set status='END' where litige=0 and fin_date < DATE_SUB(now(), INTERVAL ".$config["jours_contest"]." DAY)");

// en cas de litige, le territoire disputé redevient libre après le temps imparti
if ($config["litige_penalite"] == "0") {
    sqlexec("update duels set gagnant=0, status='END' where status != 'END' and litige=1 and fin_date < DATE_SUB(now(), INTERVAL ".$config["jours_litige"]." DAY)");
} else {
    $penalites_a_appliquer=sqlexec("select attaquant, if(duels.zone_id,'zones','keypoints') as type_territoire, if(duels.zone_id,zone_id,keypoint_id) as id_territoire from duels where status != 'END' and litige=1 and fin_date < DATE_SUB(now(), INTERVAL ".$config["jours_litige"]." DAY); update duels set gagnant=0, status='END' where status != 'END' and litige=1 and fin_date < DATE_SUB(now(), INTERVAL ".$config["jours_litige"]." DAY)");
    foreach($penalites_a_appliquer as $penalite) {
        sqlexec("select @penalite := points from $penalite[type_territoire] where id = $penalite[id_territoire]; update joueurs set point_penalite=point_penalite+@penalite where id=$penalite[attaquant];");
    }
}

// mise a jour des proprietaire lorsqu'un duel est fini
sqlexec("update zones left join duels on zones.duel_id=duels.id set zones.proprietaire=IFNULL(duels.gagnant, 0), zones.duel_id = null where duels.status='END'");
sqlexec("update keypoints left join duels on keypoints.duel_id=duels.id set keypoints.proprietaire=IFNULL(duels.gagnant, 0), keypoints.duel_id = null where duels.status='END'");
// reset du nombre de revendication d'un joueur
sqlexec("update joueurs set num_revendication=0 where ifnull(date(last_revendication), 1) < DATE_SUB(now(), INTERVAL $config[delais_revendications] DAY)");

//Règles du jeu
$regles_delais_revendic = "Les revendications se font les ";
if (strpos($config["jours_revendication"], "Monday") !== false) {
    $regles_delais_revendic .= "lundis, ";
}
if (strpos($config["jours_revendication"], "Tuesday") !== false) {
    $regles_delais_revendic .= "mardis, ";
}
if (strpos($config["jours_revendication"], "Wednesday") !== false) {
    $regles_delais_revendic .= "mercredis, ";
}
if (strpos($config["jours_revendication"], "Thursday") !== false) {
    $regles_delais_revendic .= "jeudis, ";
}
if (strpos($config["jours_revendication"], "Friday") !== false) {
    $regles_delais_revendic .= "vendredis, ";
}
if (strpos($config["jours_revendication"], "Saturday") !== false) {
    $regles_delais_revendic .= "samedis, ";
}
if (strpos($config["jours_revendication"], "Sunday") !== false) {
    $regles_delais_revendic .= "dimanches, ";
}
if ($config["jours_revendication"] == "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday") {
    $regles_delais_revendic = "Les revendications se font ";
}
$regles_delais_revendic.="dans la limite de $config[nb_revendication] territoire";
if ($config["nb_revendication"] != "1") {
    $regles_delais_revendic.="s";
}
if ($config["delais_revendications"] == "1") {
    $regles_delais_revendic.=" par jour.";
} else {
    $regles_delais_revendic.=" tous les $config[delais_revendications] jours.";
}
if ($config["jours_revendication"] == "") {
    $regles_delais_revendic = "Les revendications sont actuellement stoppés";
}
$msg_penalite="";
if ($config["litige_penalite"] == "1") {
    $msg_penalite=" et l'attaquant subira des points de pénalité";
}


if ($config["print_date_eliminatoires"] == "0"){
    $regles_eliminatoires = "";
} else {
    $regles_eliminatoires = "La phase éliminatoire du jeu commence le ".$config["print_date_eliminatoires"].". A partir de cette date, si vous perdez tous vos territoires, vous ne pourrez plus revendiquer de territoires.</br></br>";
}


if ($config["print_date_inscription"] != "0" && $config["print_date_fin_inscription"] != "0") {
    $regles_inscription = "Inscriptions ouvertes du $config[print_date_inscription] au $config[print_date_fin_inscription]</br></br>";
} else if ($config["print_date_inscription"] != "0") {
    $regles_inscription = "Inscriptions ouvertes a partir du $config[print_date_inscription]</br></br>";
} else if ($config["print_date_fin_inscription"] != "0") {
    $regles_inscription = "Inscriptions ouvertes jusqu'au $config[print_date_fin_inscription]</br></br>";
} else {
    $regles_inscription="";
}
if ($config["print_date_login"] != "0" && $config["print_date_end"] != "0") {
    $regles_login = "Le jeu est ouvert du $config[print_date_login] au $config[print_date_end]</br></br>";
} else if ($config["print_date_login"] != "0") {
    $regles_login = "Le jeu est ouvert a partir du $config[print_date_login]</br></br>";
} else if ($config["print_date_end"] != "0") {
    $regles_login = "Le jeu se termine le $config[print_date_end]</br></br>";
} else {
    $regles_login="";
}
$heures_def_duel=intval($config[jours_def_duel])*24;
$heures_contest=intval($config[jours_contest])*24;
$jours_duel_annule=intval($config[jours_def_duel])*2;
$regles = <<<REGLES
<section id="les_regles" class="wrapper style3">
				<div class="container">
					<header class="major">
						<h2>Les règles du jeu</h2>
						<p>Vous inquietez pas c'est facile !</p>
					</header>
				</div>
				<div class="container">
					<p>Le principe est très simple : une carte de Valence est disponible ci dessus.</br>Cette carte est divisée en plusieurs territoires, correspondant aux quartiers, villages et environs de Valence. On retrouve également des points clés de la vie étudiante valentinoise.</br>Le but va être de s'affronter pour conquérir des territoires et des points clés aux dépens des voisins.
Et en l'occurence, on va pas se taper sur la gueule, mais se lancer des duels en tous genres pour récupérer les territoires.</br></br>Il existe 36 zones et 30 points clés. Chaque zone vaut 1 point, et chaque point clé en vaut un certain nombre :</br>
<span style="margin-left: 5em;">&bull; 1 point par point lambda</span></br>
<span style="margin-left: 5em;">&bull; 2 points pour une école</span></br>
<span style="margin-left: 5em;">&bull; 3 points pour les endroits où on se taule</span></br>
<span style="margin-left: 5em;">&bull; 5 points pour le Partiel, les Voiliers et la Halle du Challenge.</span></br></br>$regles_delais_revendic</br></br>
Vous pouvez revendiquer au choix, s'il n'est pas déjà attaqué par un autre joueur:</br>
<span style="margin-left: 5em;">&bull; Un territoire adjacent au votre</span></br>
<span style="margin-left: 5em;">&bull; Le territoire qui contient un point clé que vous possédez</span></br>
<span style="margin-left: 5em;">&bull; Un point clé, n'importe lequel.</span></br></br>
Vous revendiquez un territoire/point clé vide ? Félicitations, il est à vous.</br>
Vous revendiquez un territoire/point clé occupé ? Un duel est alors lancé, et le défenseur a $heures_def_duel h pour fixer les termes du duel.</br>
Vous revendiquez un territoire/point clé que vous pensiez vide mais qui est en fait occupé ? Tant pis pour vous, ça fait un duel quand même.</br></br>
Lorsqu'un duel est lancé, le défenseur a $heures_def_duel h pour fixer les termes du duel, s'il ne le fait pas dans les temps, c'est l'attaquant qui aura $heures_def_duel h pour fixer les termes du duel.</br>
Si aucun des joueurs n'a fixé les termes du duel après $jours_duel_annule jours, le duel est annulé.</br>
Une fois les termes du duel fixés, vous avez $config[jours_defi] jours pour le faire et désigner un vainqueur.</br>
Si au bout de $config[jours_defi] jours, personne n'a désigné de vainqueur, le joueur qui a fixé les termes du duel sera nommé vainqueur.</br>
Si un joueur se déclare vainqueur d'un duel, son adversaire a $heures_contest h pour confirmer ou infirmer cette victoire avant que cette ci soit définitive</br>
En cas de désaccord, les 2 joueurs auront $config[jours_litige] jours pour se mettre d'accord et désigner tous les deux le même vainqueur ou le territoire disputé redeviendra libre $msg_penalite.</br></br>
$regles_eliminatoires
$regles_login
$regles_inscription
Le but du jeu est bien entendu d'avoir le plus de points.</p>
				</div>
			</section>
REGLES;

$footer = <<<FOOTER
<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="8u 12u$(medium)">
							<ul class="copyright">
								<li>&copy; 2018 Game of Valence. Tout droits réservés.</li>
								<li>Design: <a href="http://templated.co">TEMPLATED</a></li>
								<li>Images: <a href="http://unsplash.com">Unsplash</a></li>
							</ul>
						</div>
						<div class="4u$ 12u$(medium)">
							<ul class="icons">
								<li>
									<a href="https://www.facebook.com/groups/308885969625143/" class="icon rounded fa-facebook"><span class="label">Facebook</span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
FOOTER;

?>