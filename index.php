<?php
include('php/config.php');

?>
<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Game of <?php echo $nom_ville; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
        <noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<link rel="stylesheet" href="css/leaflet.css" />
        <script src="js/leaflet.js"></script>
        <script>
        var tile_server = '<?php echo $config["tile_server"] ?>';
        var tile_attribution = '<?php echo $config["tile_attribution"] ?>';
        </script>
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header">
				<h1 id="header_site_name"><a href="index.php">Game of <?php echo $nom_ville; ?></a></h1>
				<nav id="nav">
					<ul>
						<li><a href="#la_carte">La Carte</a></li>
						<li><a href="#les_territoires">Les Territoires attaquables</a></li>
						<li><a href="#le_classement">Le Classement</a></li>
						<li><a href="#les_regles">Les Règles</a></li>
						<li><a href="espace-joueurs.php" class="button special">Espace Joueurs</a></li>
					</ul>
				</nav>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h2>Game of <?php echo $nom_ville; ?></h2>
				<p>Saurez vous conquérir <?php echo $nom_ville; ?>?</p>
			</section>

		<!-- One -->
			<section id="la_carte" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>La carte</h2>
						<p>mise à jour en temps réel !</p>
					</header>
					<div id="map_container" class="row container" style="margin:auto;padding:0;">
						<div id="map" ></div>
					</div>
				</div>
			</section>
		<!-- Two -->
			<section id="les_territoires" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2>Liste des territoires attaquables</h2>
						<p id="ssTitre_Territoires_attaquables"></p>
					</header>
					<section class="profiles">
						<div id="Territoires_attaquables">
						</div>
					</section>
				</div>
			</section>
		<!-- Three -->
			<section id="le_classement" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Le classement</h2>
						<p>mis à jour en temps réel !</p>
					</header>
					<section class="profiles">
						<div id="Classement">
						</div>
					</section>
				</div>
			</section>
		<!-- Four -->
			<?php echo $regles; ?>
		<!-- Footer -->
			<?php echo $footer; ?>
    <script src="js/index.js"></script>
	</body>
</html>