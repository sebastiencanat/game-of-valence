<?php
session_start();
include('php/config.php');
actualiser_session();
if (isset($_POST["action"]) && $_POST["action"] == "change_password" && isset($_POST["newpassword"]) && isset($_POST["password"]) && hash('sha512',$_POST["password"]) == $_SESSION["password"]) {
	sqlexec("update joueurs set password='".hash('sha512',$_POST["newpassword"])."' where id=".$_SESSION["id"].";");
	actualiser_session();
}
if (isset($_POST["action"]) && $_POST["action"] == "delete_account" && isset($_POST["password"]) && hash('sha512',$_POST["password"]) == $_SESSION["password"] && $_SESSION["id"] != 0) {
	sqlexec("delete from joueurs where id=".$_SESSION["id"].";");
	actualiser_session();
}

?>
<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Game of <?php echo $nom_ville; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<script src="lib/login.js"></script>
		
	</head>
	<body class="landing">

		<!-- Header -->
        <?php if ($_SESSION["id"] != 0) { echo <<<HEADER
			<header id="header">
				<h1><a href="espace-joueurs.php">Game of $nom_ville</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="espace-joueurs.php">Espace joueurs</a></li>
						<li><a href="espace-joueurs.php#la_carte">La Carte</a></li>
						<li><a href="espace-joueurs.php#revendications">Revendiquer un territoire</a></li>
						<li><a href="espace-joueurs.php#duels">Duels</a></li>
						<li><a href="espace-joueurs.php#le_classement">Le Classement</a></li>
						<li><a href="espace-joueurs.php#les_regles">Les Règles</a></li>
						<li><a href="logout.php" class="button special">Déconnexion</a></li>
					</ul>
				</nav>
			</header>
HEADER;
                                        } else { echo <<<HEADER
        
        <header id="header">
			<h1 id="header_site_name"><a href="admin.php">Game of $nom_ville</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="admin.php">Espace d'administration</a></li>
						<li><a href="admin.php#tous_les_duels">Duels</a></li>
						<li><a href="admin.php#liste_joueurs">Joueurs</a></li>
						<li><a href="admin.php#points cles">Points Clés</a></li>
						<li><a href="admin.php#zones">Zones</a></li>
						<li><a href="admin.php#config">Configuration</a></li>
						<li><a href="admin.php#reset_game">Réinitialisation</a></li>
						<li><a href="logout.php" class="button special">Déconnexion</a></li>
					</ul>
				</nav>
			</header>
HEADER;
		} ?>
		<!-- Banner -->
			<section id="banner">
				<h2>Paramètres du compte <?php echo $_SESSION["nom"]; ?></h2>
				
			</section>
			
		<!-- One -->
			<section id="section_modify_password" class="wrapper style3 special">
				<div class="container">
					<header class="major">
						<h2>Modifier mon mot de passe</h2>
						<p id="ssTitreLogin">Vous devrez vous reconnecter</p>
					</header>
				</div>
				<div class="container 50%">
					<form id="change-password" action="#" method="post">
					<input name="action" value="change_password" type="hidden">
						<div class="row uniform">
							<div class="12u">
								<input name="password" id="old-password" value="" placeholder="Ancien mot de passe" type="password">
							</div>
						</div><div class="row uniform">
							<div class="12u">
								<input name="newpassword" id="new-password" value="" placeholder="Nouveau mot de passe" type="password">
							</div>
							<div class="12u$">
								<ul class="actions">
									<li><input value="Changer le mot de passe" class="special big" type="submit"></li>
								</ul>
							</div>
						</div>
					</form>
				</div>
			</section>
	
		<!-- two -->
		<?php if ($_SESSION["id"] != 0) { echo <<<DELETEACCOUNT
			<section id="section_delete_account" class="wrapper style6 special">
				<div class="container">
					<header class="major">
						<h2>Supprimer mon compte</h2>
						<p>Cette action supprimera toute trace de votre compte, donnera vos territoires à leurs attaquants respectifs ou, à défaut les rendra disponibles</p>
					</header>
				</div>
				<div class="container 50%">
					<form id="delete-account" action="#" method="post">
					<input name="action" value="delete_account" type="hidden">
						<div class="row uniform">
							<div class="12u">
								<input name="password" id="password" value="" placeholder="Mot de passe" type="password">
							</div>
							<div class="12u$">
								<ul class="actions">
									<li><input value="Supprimer mon compte" class="special big" type="submit"></li>
								</ul>
							</div>
						</div>
					</form>
				</div>
			</section>
DELETEACCOUNT;
		} ?>
		
		<?php echo $footer; ?>
	</body>
</html>